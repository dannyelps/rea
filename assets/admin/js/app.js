$(document).ready(function () {
    /* Load tootip*/
    $('[data-toggle="tooltip"]').tooltip();

    /* Load default CKEditor plugin*/
    ClassicEditor
        .create(document.querySelector('#editor'), {
            toolbar: ['bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote', 'link']
        })
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.log(error);
        });

    var allEditors = document.querySelectorAll('.editor');
    for (var i = 0; i < allEditors.length; ++i) {
        ClassicEditor
            .create(allEditors[i], {
                toolbar: ['bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote', 'link']
            });
    }

    /* Load default Dropzone plugin*/
    Dropzone.options.imageDropzone = {
        //paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 2, // MB
        acceptedFiles: 'image/*',
        dictDefaultMessage: "Arraste imagens ou clique aqui para fazer upload",
        dictFallbackMessage: "O seu browser não suporta a funcionalidade drag'n'drop (arrastar).",
        dictFallbackText: "Utilize o formulário abaixo para fazer o upload de seus ficheiros.",
        dictFileTooBig: "O ficheiro é demasiado grande({{filesize}}MB). Tamanho máximo: {{maxFilesize}}MB.",
        dictInvalidFileType: "Não pode fazer upload de ficheiros deste tipo.",
        dictResponseError: "O servidor respondeu com o código {{statusCode}}.",
        dictCancelUpload: "Cancelar upload",
        dictCancelUploadConfirmation: "Are you sure you want to cancel this upload?",
        dictCancelUploadConfirmation: "Tem acerteza que pretende cancelar o upload?",
        dictRemoveFile: "Remover ficheiro",
        dictRemoveFileConfirmation: null,
        dictMaxFilesExceeded: "Não pode enviar mais ficheiros",
        success: function (file, response) {

            console.log(response);
            console.log(file);

            $('.uploaded-images').prepend(response.html);
            $('.uploaded-images > .no-images').remove();

            var num = parseInt($('#foto-count').text());
            $('#foto-count').text(num + 1);
        }
    };
});

