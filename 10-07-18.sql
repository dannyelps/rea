-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 10-Jul-2018 às 16:24
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rea`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `accessories`
--

DROP TABLE IF EXISTS `accessories`;
CREATE TABLE IF NOT EXISTS `accessories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `accessories`
--

INSERT INTO `accessories` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Bateria', '2018-06-29 17:25:13', '2018-06-29 17:25:13', NULL),
(2, 'Tampa da Lente', '2018-06-29 17:25:43', '2018-06-29 17:25:43', NULL),
(3, 'Manual impresso', '2018-07-04 12:55:33', '2018-07-04 12:55:33', NULL),
(4, 'chave de aperto', '2018-07-04 12:55:57', '2018-07-04 12:55:57', NULL),
(5, 'Difusor', '2018-07-04 12:56:17', '2018-07-04 12:56:17', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `catalogs`
--

DROP TABLE IF EXISTS `catalogs`;
CREATE TABLE IF NOT EXISTS `catalogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `catalogs`
--

INSERT INTO `catalogs` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Material do DI', 'material-do-di', '2018-07-04 13:06:38', '2018-07-04 13:06:45', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `catalogs_equipments`
--

DROP TABLE IF EXISTS `catalogs_equipments`;
CREATE TABLE IF NOT EXISTS `catalogs_equipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catalog_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Câmera DSLR', '2018-06-29 17:19:14', '2018-06-29 17:19:14', NULL),
(2, 'Câmera de Video', '2018-06-29 17:19:19', '2018-06-29 17:19:19', NULL),
(3, 'Tripé', '2018-06-29 17:19:25', '2018-06-29 17:19:25', NULL),
(4, 'Iluminação', '2018-06-29 17:19:30', '2018-06-29 17:19:30', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories_accessories`
--

DROP TABLE IF EXISTS `categories_accessories`;
CREATE TABLE IF NOT EXISTS `categories_accessories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `accessory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categories_accessories`
--

INSERT INTO `categories_accessories` (`id`, `category_id`, `accessory_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 1, 2),
(4, 2, 2),
(5, 1, 3),
(6, 2, 3),
(7, 3, 4),
(8, 4, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `equipments`
--

DROP TABLE IF EXISTS `equipments`;
CREATE TABLE IF NOT EXISTS `equipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `requirements` text,
  `datasheet` text,
  `manuals` varchar(255) DEFAULT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `model` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `equipments`
--

INSERT INTO `equipments` (`id`, `name`, `slug`, `description`, `full_name`, `requirements`, `datasheet`, `manuals`, `other_info`, `model`, `brand`, `category_id`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Canon 80D', '', '<p> </p>', ' Canon 80D', '<p> </p>', '<p> </p>', '<p> </p>', '<p> </p>', '80D', 'Canon', 1, 1, '2018-06-29 17:25:57', '2018-07-05 17:50:17', NULL),
(2, 'Canon 80D ', 'camera-dslr-canon-80d', NULL, 'Câmera DSLR Canon 80D', NULL, NULL, NULL, NULL, '80D', 'Canon', 1, 1, '2018-07-01 18:34:04', '2018-07-05 21:03:27', NULL),
(3, 'Canon 80D', 'camera-dslr-canon-80d-1', '<p>&nbsp;</p>', 'Câmera DSLR Canon 80D', '<p>Bateria carregada</p>', '<p>&nbsp;</p>', '<p>&nbsp;</p>', '<p>&nbsp;</p>', '80D', 'Canon', 1, 1, '2018-07-04 12:53:40', '2018-07-04 13:44:44', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `equipments_accessories`
--

DROP TABLE IF EXISTS `equipments_accessories`;
CREATE TABLE IF NOT EXISTS `equipments_accessories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accessory_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `equipments_accessories`
--

INSERT INTO `equipments_accessories` (`id`, `accessory_id`, `equipment_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 1, 3),
(4, 2, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `equipments_img`
--

DROP TABLE IF EXISTS `equipments_img`;
CREATE TABLE IF NOT EXISTS `equipments_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `fileurl` varchar(255) NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `equipments_img`
--

INSERT INTO `equipments_img` (`id`, `equipment_id`, `filename`, `fileurl`, `is_visible`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '6860300470_624e0541a5_b.jpg', 'upload/equipments/jezreims_6860300470_624e0541a5_b.jpg', 1, '2018-07-05 21:10:29', '2018-07-05 21:10:29', '2018-07-05 21:10:29'),
(2, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/equipments/kwg0sz7v_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-05 21:17:02', '2018-07-05 21:17:02', '2018-07-05 21:17:02'),
(3, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/equipments/0gy8szex_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-05 21:30:38', '2018-07-05 21:30:38', '2018-07-05 21:30:38'),
(4, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/equipments/xszkv2t6_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-05 21:30:55', '2018-07-05 21:30:55', '2018-07-05 21:30:55'),
(5, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/equipments/e2ouaxk4_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-05 21:31:00', '2018-07-05 21:31:00', '2018-07-05 21:31:00'),
(6, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/equipments/sliwvrcj_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-07 14:23:05', '2018-07-07 13:23:05', '2018-07-07 13:23:05'),
(7, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/equipments/vnlg2xh9_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-07 14:23:03', '2018-07-07 13:23:03', '2018-07-07 13:23:03'),
(8, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/equipments/er8fjgpe_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-07 14:22:59', '2018-07-07 13:22:59', '2018-07-07 13:22:59'),
(9, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/equipments/gyswdd6h_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-07 14:22:57', '2018-07-07 13:22:57', '2018-07-07 13:22:57'),
(10, 1, 'Logotipo_MaraPedro-Branco.png', 'upload/equipments/yhsxgskz_logotipo_marapedro-branco.png', 1, '2018-07-07 10:33:57', '2018-07-07 10:33:57', '2018-07-07 10:33:57'),
(11, 1, '1.jpg', 'upload/equipments/sphdl9ut_1.jpg', 1, '2018-07-07 14:22:55', '2018-07-07 13:22:55', '2018-07-07 13:22:55'),
(12, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/equipments/421fe385_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-07 14:27:39', '2018-07-07 13:27:39', '2018-07-07 13:27:39'),
(13, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/equipments/9klx3nid_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-07 14:29:48', '2018-07-07 13:29:48', '2018-07-07 13:29:48'),
(14, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/equipments/escf3uqp_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-07 13:29:38', '2018-07-07 13:29:38', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Acesso à plataforma de gestão'),
(2, 'dev', 'Acesso a zonas de desenvolvimento e funções');

-- --------------------------------------------------------

--
-- Estrutura da tabela `reports`
--

DROP TABLE IF EXISTS `reports`;
CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fileurl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `reports`
--

INSERT INTO `reports` (`id`, `name`, `description`, `filename`, `fileurl`, `is_read`, `status`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Limites de texto nos campos', '<ul><li>Sem verificação mínima e máxima de caracteres nos campos</li></ul>', NULL, NULL, 0, 'Em espera', 1, '2018-07-03 10:01:06', '2018-07-05 16:48:47', NULL),
(2, 'Upload de imagens', '<ul><li>Não é reconhecido o diretório de upload</li><li>Imagem de erro não é carregada para substituir imagem.</li></ul>', NULL, NULL, 0, 'Em espera', 1, '2018-07-05 23:53:15', '2018-07-05 23:53:15', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tips`
--

DROP TABLE IF EXISTS `tips`;
CREATE TABLE IF NOT EXISTS `tips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `text` text CHARACTER SET latin1,
  `catalog_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tips`
--

INSERT INTO `tips` (`id`, `name`, `slug`, `description`, `text`, `catalog_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Fotografia de produto', 'fotografia-de-produto', 'Fotografia de proximidade, com o objetivo de captar e eventualmente salientar as caraterísticas de um objeto.', '<p>Lorem <a href=\"https://www.bigcommerce.com/blog/how-to-rock-product-photography-on-a-budget/\">Ipsum</a></p><p>&nbsp;</p>', NULL, '2018-07-04 13:06:13', '2018-07-07 18:14:33', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tips_img`
--

DROP TABLE IF EXISTS `tips_img`;
CREATE TABLE IF NOT EXISTS `tips_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tips_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `fileurl` varchar(255) NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tips_img`
--

INSERT INTO `tips_img` (`id`, `tips_id`, `filename`, `fileurl`, `is_visible`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/tips/fs1jhrmh_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-07 17:30:08', '2018-07-07 17:30:08', NULL),
(2, 1, '1 eos80d-ef-s18-135-3q-.jpg', 'upload/tips/k4vbhcvh_1_eos80d-ef-s18-135-3q-.jpg', 1, '2018-07-07 17:31:01', '2018-07-07 17:31:01', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `_token` varchar(40) DEFAULT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `ip_address` varchar(15) NOT NULL,
  `salt` varchar(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `_token`, `last_login`, `is_active`, `ip_address`, `salt`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Daniel Seabra', 'dev@rea.com', '$2y$10$AkMoV7ij.hJKUHUIzP8n.umWKItYNfHcaVhjU.4rj0m0lgxdXNomi', '5de708f9ee8ea1c75ffd5583ea6b03e7', 1530979393, 1, '::1', NULL, '2018-06-29 18:15:00', '2018-07-07 15:03:13', NULL),
(2, 'Administrador', 'admin@rea.com', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', NULL, NULL, 1, '', NULL, '2018-06-29 18:15:00', '2018-07-03 23:24:56', NULL),
(3, 'DI@Valter', 'valter@estgv.ipv.pt', '$2y$10$NHtla/3HJUzlXuoYgVxvSOKCRo8WVPYMKIQOITy.xbZyT0SvO0Yi6', NULL, 1530708583, 1, '217.129.163.7', NULL, '2018-07-03 21:34:57', '2018-07-04 12:49:43', NULL),
(4, 'João Sá', 'joaosa_95@hotmail.com', '$2y$10$J2EhuUhomZJHThK3mI7jbeld0XtUou7Rqa2uyO/1vmvgJZo2HCZBW', NULL, 1530812752, 1, '193.137.7.11', NULL, '2018-07-03 21:48:08', '2018-07-05 17:45:52', NULL),
(5, 'José Veiga', 'veiga757@gmail.com', '$2y$10$i6xbxKSEtMaX0DtqER99ZeJNSWjpKxAzyY6Kf8z6oP3KGW9Zt.qeW', NULL, 1530654856, 1, '193.137.7.11', NULL, '2018-07-03 21:49:36', '2018-07-03 21:54:16', NULL),
(6, 'Céline Direito', 'celinedireito@hotmail.com', '$2y$10$V6O37.xcFX/PMG5AFz5GhOJWQcxXGJwmX0v49S67KEA/VUNS22S8q', NULL, 1530692331, 1, '188.81.31.191', NULL, '2018-07-03 21:51:32', '2018-07-04 08:18:51', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users_permissions`
--

DROP TABLE IF EXISTS `users_permissions`;
CREATE TABLE IF NOT EXISTS `users_permissions` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `permission_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users_permissions`
--

INSERT INTO `users_permissions` (`id`, `user_id`, `permission_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 3, 1),
(7, 4, 1),
(8, 5, 1),
(9, 6, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
