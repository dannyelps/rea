<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dicas extends Public_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['tips'] = Tip::all();
        $this->data['tipsImgs'] = TipPhoto::all();


        $this->template->home_render('public/tips/index', $this->data);
    }

    public function show($slug = null)
    {
        if (empty($slug)) {
            $result = [
                'feedback' => 'Não existe essa dica',
                'type' => 'error',
                'result' => 'true'
            ];

            /* Return feedback */
            $this->session->set_flashdata('result', $result);
            redirect('dicas');
        }

        $tip = Tip::where('slug', $slug)->first();

        if (!$tip) {
            $result = [
                'feedback' => 'Não existe essa dica',
                'type' => 'error',
                'result' => 'true'
            ];

            /* Return feedback */
            $this->session->set_flashdata('result', $result);
            redirect('dicas');
        }

        $this->data['tipImgs'] = TipPhoto::where('tips_id', $tip->id)->get();
        $this->data['tip'] = $tip;

        $this->template->home_render('public/tips/show', $this->data);
    }

    public function search($search = null)
    {

        $this->data['tips'] = Tip::where('name', 'LIKE', '%' . $search . '%')->get();
        $this->data['tipsImgs'] = TipPhoto::all();
        $this->data['search'] = $search;

        $this->template->home_render('public/tips/index', $this->data);
    }
}
