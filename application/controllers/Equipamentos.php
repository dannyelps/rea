<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipamentos extends Public_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        /* Set Data */
        $this->data['equipments'] = Equipment::all();
        $this->data['equipmentsImgs'] = EquipmentPhoto::all();

        $this->template->home_render('public/equipments/index', $this->data);
    }

    public function show($slug = null)
    {
        /* If don´t exist show error */
        if (empty($slug)) {
            $result = [
                'feedback' => 'Não existe esse equipamento',
                'type'     => 'error',
                'result'   => 'true'
            ];

            /* Return feedback */
            $this->session->set_flashdata('result', $result);
            redirect('equipamentos');
        }

        /* get data */
        $equipment = Equipment::where('slug', $slug)->first();
        if (!$equipment) {
            $result = [
                'feedback' => 'Não existe esse equipamento',
                'type'     => 'error',
                'result'   => 'true'
            ];

            /* Return feedback */
            $this->session->set_flashdata('result', $result);
            redirect('equipamentos');
        }

        /* Set data */
        $this->data['equipmentImgs'] = EquipmentPhoto::where('equipment_id', $equipment->id)->get();
        $this->data['equipment'] = $equipment;

        $this->template->home_render('public/equipments/show', $this->data);
    }

    public function search($search = null)
    {

        $this->data['equipments'] = Equipment::where('name', 'LIKE', '%' . $search . '%')->get();
        $this->data['equipmentsImgs'] = EquipmentsPhotos::all();
        $this->data['search'] = $search;

        $this->template->home_render('public/equipments/index', $this->data);
    }
}
