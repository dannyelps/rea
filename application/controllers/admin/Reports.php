<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Title Page :: Common */
        $this->page_title->push('Problemas');
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'Problemas', 'admin/reports');

        /* Permission */
        $this->permission = 'admin';

        /* Verify Permission */
        if (!$this->ion_auth->in_group($this->permission)) {
            $result = [
                'feedback' => 'Não possui permissão para aceder a esta página.',
                'type' => 'error',
                'result' => 'true'
            ];

            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }
    }

    /**
     * Display a listing of the equipments.
     *
     * @return template -> render a view
     */
    public function index()
    {
        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Load Template */
        $this->template->admin_render('admin/reports/index', $this->data);
    }

    /*
     * Redirect to update with id null
     */
    public function store()
    {
        return $this->update($id = null);
    }

    /**
     * Update the specified equipament in storage.
     *
     * @param  $this ->input->post()
     * @param  int $id
     * @return redirect to view
     */
    public function update($id = null)
    {
        $action = empty($id) ? '' : 'edit/' . $id;

        //Get equipment or add a new one
        $report = Report::findOrNew($id);

//        //only dev's set status
//        if(!$this->ion_auth->in_group('dev')) {
//
//        }

        //Get Post data
        $input = $this->input->post();

        /* Rules of input */
        $this->form_validation->set_rules('name', 'Nome', 'trim|required|min_length[3]|max_length[254]');
        $this->form_validation->set_rules('description', 'Descrição', 'trim|max_length[254]');

        //Only validate input if exist
        if (!empty($input)) {

            // Validate input
            if ($this->form_validation->run() == TRUE) {

                if (isset($_FILES['file'])) {
                    /* Get File data */
                    $file = $_FILES['file'];

                    /* Upload File*/
                    if (!$file->upload($input)) {
                        $result = [
                            'feedback' => 'Problema ao enviar o print.',
                            'type' => 'error',
                            'result' => 'true'
                        ];
                        redirect('admin/');
                    }
                } else {
                    unset($input['file']);
                }

                if ($action == '') {
                    $input['status'] = 'Em espera';
                }

                //Mass assignment and save
                $report->fill($input);
                $report->save();

                //Set feedback
                if ($action == '') {
                    $result = [
                        'feedback' => 'Problema enviado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                } else {
                    $result = [
                        'feedback' => 'Problema #' . $report->name . ' editado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                    $action = 'reports';
                }

                //Back to index page with result
                $this->session->set_flashdata('result', $result);
                redirect('admin/' . $action);
            } else {

                //Set feedback
                $result = [
                    'feedback' => validation_errors(),
                    'type' => 'error',
                    'result' => 'true'
                ];

                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('report', $input);
                redirect('admin/reports/' . $action);
            }
        }

        //Set feedback
        $result = [
            'feedback' => 'Campos por preencher.',
            'type' => 'error',
            'result' => 'true'
        ];

        /* Return feedback */
        $this->session->set_flashdata('report', $input);
        $this->session->set_flashdata('result', $result);
        redirect('admin/report/' . $action);
    }

    /**
     * Load the view to edit catalog in storage.
     *
     * @param  $id
     * @return Render view
     */
    public function edit($id)
    {
        /* Verify if id came in form */
        if (empty($id)) {
            redirect('admin/reports');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, 'Editar Problema', 'admin/reports/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Get data from catalog */
        $this->data['report'] = Report::where('id', $id)->first();

        /* Check if exist equipment */
        if (empty($this->data['report'])) {
            redirect('admin/reports');
        }

        /* Set read to true */
        if($this->ion_auth->is_dev()) {
            Report::where('id', $id)->update(['is_read' => 1]);
        }

        /* Load Template */
        $this->template->admin_render('admin/reports/edit', $this->data);
    }

    /**
     * Delete the specified user in storage.
     *
     * @param  $id
     * @return redirect to view
     */
    public function delete($id)
    {
        /* Remove */
        $deleted = Report::where('id', $id)->delete();

        /* Set feedback */
        if ($deleted) {
            $result = [
                'feedback' => 'Problema eliminado com sucesso.',
                'type' => 'success',
                'result' => 'true'
            ];
        } else {
            $result = [
                'feedback' => 'Erro ao eliminar o Problema.',
                'type' => 'error',
                'result' => 'true'
            ];
        }

        /* Load to a temporary session feedback and redirect */
        $this->session->set_flashdata('result', $result);
        redirect('admin/reports');
    }

    /* Set datatables of reports */
    public function datatables()
    {
        /* Get all catalogs */
        $reports = Report::get();

        $arr = [];

        /* Load data to a array */
        foreach ($reports as $key => $value) {
            $row = [];
            $row[] = $value->id;

            //Change read status
            if ($value->is_read)
                if($this->ion_auth->is_dev())
                    $row[] = '<a href="' . base_url() .'admin/reports/notread/' . $value->id . '"><span class="label label-success"><i class="fa fa-eye"></i></span></a>';
                else
                    $row[] = '<span class="label label-success"><i class="fa fa-eye"></i></span>';
            else
                if ($this->ion_auth->is_dev())
                    $row[] = '<a href="' . base_url() .'admin/reports/read/' . $value->id . '"><span class="label label-default"><i class="fa fa-eye-slash"></i></span></a>';
                else
                    $row[] = '<span class="label label-default"><i class="fa fa-eye-slash"></i></span>';

            $row[] = anchor('admin/reports/edit/' . $value->id, $value->name);
            $row[] = $value->description;
            $row[] = '<span class="label label-default">' . ucwords($value->status) . '</span>';
            $row[] = Carbon\Carbon::parse($value->created_at)->toDateTimeString();
            $row[] = anchor('admin/reports/edit/' . $value->id, '<i class="fa fa-pencil"></i>', array('class' => 'btn btn-default')) . '<span class="margin-left-5"></span>' .
                '<a href="' . base_url('admin/reports/delete/') . $value->id . '" class="btn btn-danger">
                            <i class="fa fa-remove"></i></a>';

            $arr[] = $row;
        }

        $output = [
            "data" => $arr,
        ];

        echo json_encode($output);
    }

    /**
     * Change read status of a report on storage
     *
     * @param  $id
     * @return redirect to view
     */
    public function notread($id)
    {
        /* Change status */
        $teste = Report::where('id', $id)->update(['is_read' => 0]);

        /* Set feedback */
        if ($teste) {
            $result = [
                'feedback' => 'Estado de visualização alterado com sucesso.',
                'type'     => 'success',
                'result'   => 'true'
            ];
        } else {
            $result = [
                'feedback' => 'Erro ao alterar estado de visualização.',
                'type'     => 'error',
                'result'   => 'true'
            ];
        }

        /* Load to a temporary session feedback and redirect */
        $this->session->set_flashdata('result', $result);
        redirect('admin/reports');
    }


    /**
     * Change read status of a report on storage
     *
     * @param  $id
     * @return redirect to view
     */
    public function read($id)
    {
        /* Change status */
        $teste = Report::where('id', $id)->update(['is_read' => 1]);

        /* Set feedback */
        if ($teste) {
            $result = [
                'feedback' => 'Estado de visualização alterado com sucesso.',
                'type'     => 'success',
                'result'   => 'true'
            ];
        } else {
            $result = [
                'feedback' => 'Erro ao alterar estado de visualização.',
                'type'     => 'error',
                'result'   => 'true'
            ];
        }

        /* Load to a temporary session feedback and redirect */
        $this->session->set_flashdata('result', $result);
        redirect('admin/reports');
    }
}
