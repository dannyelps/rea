<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();


        /* Title Page :: Common */
        $this->page_title->push(lang('word.categories'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('word.categories'), 'admin/categories');

        /* Permission */
        $this->permission = 'categories';

        /* Verify Permission */
        if (!$this->ion_auth->in_group($this->permission) && !$this->ion_auth->is_admin()) {
            $result = [
                'feedback' => 'Não possui permissão para aceder a esta página.',
                'type'     => 'error',
                'result'   => 'true'
            ];

            /* Return feedback */
            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }
    }

    /**
     * Display a listing of the Categories.
     * @return template -> render a view
     */
    public function index()
    {
        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Load Template */
        $this->template->admin_render('admin/categories/index', $this->data);
    }

    /*
     * Load breadcrumbs and page to create Accessory
     * @return Render a view
     */
    public function create()
    {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, 'Adicionar Acessório', 'admin/accessories/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Title of page */
        $this->data['action'] = 'Adicionar Categoria';

        /* Route of form*/
        $this->data['formRoute'] = 'admin/categories/store';

        /* Load Template */
        $this->template->admin_render('admin/categories/edit', $this->data);
    }

    /*
     * Redirect to update with id null
     */
    public function store()
    {
        return $this->update($id = null);
    }

    /**
     * Load the view to edit Categories in storage.
     *
     * @param  $id
     * @return Render view
     */
    public function edit($id)
    {
        /* Verify if id came in form */
        if (empty($id)) {
            redirect('admin/categories');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, 'Editar Categoria', 'admin/categories/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Get data from category */
        $this->data['category'] = Category::where('id', $id)->first();

        /* If dont exist redirect to list of tips */
        if (!$this->data['category']) {
            redirect('admin/categories');
        }

        /* Title of page */
        $this->data['action'] = 'Editar Categoria';

        /* Route of form*/
        $this->data['formRoute'] = 'admin/categories/update/' . $this->data['category']->id;

        /* Load Template */
        $this->template->admin_render('admin/categories/edit', $this->data);
    }

    /**
     * Update the specified category in storage.
     *
     * @param  $this ->input->post()
     * @param  int $id
     * @return redirect to view
     */
    public function update($id = null)
    {
        $action = empty($id) ? 'create' : 'edit/' . $id;

        //Get category or Create a new one
        $category = Category::findOrNew($id);

        //Get Post data
        $input = $this->input->post();

        /* Rules of input */
        $this->form_validation->set_rules('name', 'Nome', 'trim|required|min_length[3]');

        //Only validate input if exist
        if (!empty($input)) {

            // Validate input
            if ($this->form_validation->run() == TRUE) {

                //Mass assignment and save
                $category->fill($input);
                $category->save();

                //Set feedback
                if ($action == 'create') {
                    $result = [
                        'feedback' => 'Categoria criado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                } else {
                    $result = [
                        'feedback' => 'Categoria #' . $category->name . ' editado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                }

                /* Return feedback */
                $this->session->set_flashdata('result', $result);
                redirect('admin/categories');
            }
        }

        //On error back to index page with error and post data
        $result = [
            'feedback' => validation_errors(),
            'type'     => 'error',
            'result'   => 'true'
        ];

        /* Return feedback */
        $this->session->set_flashdata('category', $input);
        $this->session->set_flashdata('result', $result);
        redirect('admin/categories/' . $action);
    }

    /* Loading table data*/

    /**
     * Delete the specified Tip in storage.
     *
     * @param  $id
     * @return redirect to view
     */
    public function delete($id)
    {
        /* Remove category */
        $deleted = Category::where('id', $id)->delete();

        /* Set feedback */
        if ($deleted) {
            $result = [
                'feedback' => 'Categoria eliminado com sucesso.',
                'type' => 'success',
                'result' => 'true'
            ];
        } else {
            $result = ['feedback' => 'Erro ao eliminar a Categoria.',
                'type' => 'error',
                'result' => 'true'];
        }

        /* Return feedback */
        $this->session->set_flashdata('result', $result);
        redirect('admin/categories');
    }

    /* Set datatables of Categories */
    public function datatables()
    {
        /* Get all categories */
        $categories = Category::get();

        $arr = [];

        /* Load data to a array */
        foreach ($categories as $key => $value) {
            $row = [];

            $row[] = $value->id;
            $row[] = anchor('admin/categories/edit/' . $value->id, $value->name);

            $row[] = Carbon\Carbon::parse($value->created_at)->toDateTimeString();
            $row[] = anchor('admin/categories/edit/' . $value->id, '<i class="fa fa-pencil"></i>', array('class' => 'btn btn-default')) . '<span class="margin-left-5"></span>' .
                '<a href="' . base_url('admin/categories/delete/') . $value->id . '" class="btn btn-danger"><i class="fa fa-remove"></i></a>';

            $arr[] = $row;
        }

        $output = [
            "data" => $arr,
        ];

        echo json_encode($output);
    }
}
