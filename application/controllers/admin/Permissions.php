<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Title Page :: Common */
        $this->page_title->push('Permissões');
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'Permissões', 'admin/permissions');

        /* Permission */
        $this->permission = 'admin';

        /* Verify Permission */
        if (!$this->ion_auth->in_group($this->permission)) {
            $result = [
                'feedback' => 'Não possui permissão para aceder a esta página.',
                'type'     => 'error',
                'result'   => 'true'
            ];

            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }
    }

    /**
     * Display a listing of the users.
     *
     * @return template -> render a view
     */
    public function index()
    {
        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Get all users */
        $this->data['permissions'] = Permission::all();

        /* Load Template */
        $this->template->admin_render('admin/permissions/index', $this->data);
    }

    /* Set data for datatables */
    public function datatables()
    {
        /* Get all permissions */
        $permissions = Permission::get();

        $arr = [];

        /* Load data to a array */
        foreach ($permissions as $key => $value) {
            $row = [];
            $row[] = $value->id;
            $row[] = $value->name;
            $row[] = $value->description;

            $arr[] = $row;
        }

        $output = [
            "data" => $arr,
        ];

        echo json_encode($output);
    }
}
