<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipments extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Title Page :: Common */
        $this->page_title->push('Equipamentos');
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'Equipamentos', 'admin/equipments');

        /* Permission */
        $this->permission = 'admin';

        /* Verify Permission */
        if (!$this->ion_auth->in_group($this->permission)) {
            $result = [
                'feedback' => 'Não possui permissão para aceder a esta página.',
                'type' => 'error',
                'result' => 'true'
            ];

            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }
    }

    /**
     * Display a listing of the equipments.
     *
     * @return template -> render a view
     */
    public function index()
    {
        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Get all categories for modal create */
        $this->data['categories'] = Category::all();

        /* Check if exist categories */
        if ($this->data['categories']->isEmpty()) {
            $result = [
                'feedback' => 'Insira uma categoria primeiro.',
                'type' => 'error',
                'result' => 'true'
            ];

            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }

        /* Load Template */
        $this->template->admin_render('admin/equipments/index', $this->data);
    }

    /*
     * Redirect to update with id null
     */
    public function store()
    {
        return $this->update($id = null);
    }

    /**
     * Update the specified equipament in storage.
     *
     * @param  $this ->input->post()
     * @param  int $id
     * @return redirect to view
     */
    public function update($id = null)
    {
        $action = empty($id) ? '' : 'edit/' . $id;

        //Get equipment or add a new one
        $equipment = Equipment::findOrNew($id);

        //Get Post data
        $input = $this->input->post();

        //Get Accessories
        $accessories = $input['accessories'];
        unset($input['accessories']);

        /* Rules of input */
        $this->form_validation->set_rules('brand', 'Marca', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('model', 'Modelo', 'trim|required|min_length[3]');

        if ($action == '') {
            $this->form_validation->set_rules('category_id', 'Categoria', 'required');
            $categoryName = Category::where('id', $input['category_id'])->pluck('name');
            $input['full_name'] = $categoryName . ' ' . $input['name'];
        }

        //Only validate input if exist
        if (!empty($input)) {

            // Validate input
            if ($this->form_validation->run() == TRUE) {

                //Set name and full_name of equipment
                $input['name'] = $input['brand'] . ' ' . $input['model'];

                //Mass assignment and save
                $equipment->fill($input);
                $equipment->save();

                //Set slug of Tip
                $slug = $this->setSlug($input['full_name'], $equipment->id);
                Equipment::where('id', $equipment->id)
                    ->update(['slug' => $slug]);

                //Sync accessories
                if (isset($accessories)) {
                    $equipment->accessories()->sync($accessories);
                }

                //Send feedback to index page
                if ($action == '') {
                    $result = [
                        'feedback' => 'Equipamento criado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                    $action .= 'edit/' . $equipment->id;
                } else {
                    $result = [
                        'feedback' => 'Equipamento #' . $equipment->name . ' editado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                    $action = '';
                }

                //Back to index page with result
                $this->session->set_flashdata('result', $result);
                redirect('admin/equipments/' . $action);
            } else {

                //Set feedback
                $result = [
                    'feedback' => validation_errors(),
                    'type' => 'error',
                    'result' => 'true'
                ];

                /* Return feedback */
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('equipment', $input);
                redirect('admin/equipments/' . $action);
            }
        }

        //Set feedback
        $result = [
            'feedback' => 'Campos por preencher.',
            'type' => 'error',
            'result' => 'true'
        ];

        /* Return feedback */
        $this->session->set_flashdata('equipment', $input);
        $this->session->set_flashdata('result', $result);
        redirect('admin/equipments/' . $action);
    }

    public function setSlug($name, $id)
    {
        $accents = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'þ', 'ÿ');
        $noAccents = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'B', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'p', 'y');

        $name = str_replace($accents, $noAccents, $name);
        $name = strtolower($name);

        $slug = str_slug($name, "-");
        $params = array();
        $params['slug'] = $slug;
        $i = 0;

        /* Check if exist and increase number */
        while (Equipment::where('id', '!=', $id)->where('slug', $slug)->first()) {
            if (!preg_match('/-{1}[0-9]+$/', $slug)) {
                $slug .= '-' . ++$i;
            } else {
                $slug = preg_replace('/[0-9]+$/', ++$i, $slug);
            }
            $params ['slug'] = $slug;
        }

        /* Return the final slug */
        return $slug;
    }

    /**
     * Load the view to edit equipment in storage.
     *
     * @param  $id
     * @return Render view
     */
    public function edit($id)
    {
        /* Verify if id came in form */
        if (empty($id)) {
            redirect('admin/equipments');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, 'Editar Equipamento', 'admin/equipments/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Get data from equipment */
        $this->data['equipment'] = Equipment::where('id', $id)->first();

        /*Check if exist equipment*/
        if (empty($this->data['equipment'])) {
            redirect('admin/equipments');
        }

        /* Category of equipment */
        $category_id = $this->data['equipment']->category_id;

        /* Get acessories */
        $this->data['accessories'] = Accessory::whereHas('categories', function ($q) use ($category_id) {
            $q->where('category_id', $category_id);
        })->orderby('name', 'asc')->get();

        /* Get Select Categories from Accessory */
        $this->data['selectedAccessories'] = $this->data['equipment']->accessories()->get();

        /* Get images from Equipment */
        $this->data['photos'] = EquipmentPhoto::where('equipment_id', $this->data['equipment']->id)->get();

        /* Load Template */
        $this->template->admin_render('admin/equipments/edit', $this->data);
    }

    /* Set data for datatables */

    /**
     * Delete the specified user in storage.
     *
     * @param  $id
     * @return redirect to view
     */
    public function delete($id)
    {
        $deleted = Equipment::where('id', $id)->delete();

        if ($deleted) {

            /* Mass destroy of tips images */
//            $photos = EquipmentPhoto::where('equipment_id', $id)->get();

//            foreach ($photos as $photo) {
//                /* Remove image from folder */
////                unlink($photo->fileurl);
//            }

            /* Remove from DB only if exist images */
            if (EquipmentPhoto::where('equipment_id', $id)->count() > 0) {
                $deleted_images = EquipmentPhoto::where('equipment_id', $id)->delete();
            } else {
                $deleted_images = true;
            }

            /* Set feedback */
            if (!$deleted_images) {
                $result = [
                    'feedback' => 'Erro ao eliminar imagens do equipamento.',
                    'type' => 'error',
                    'result' => 'true'
                ];
            } else {
                $result = [
                    'feedback' => 'Equipamento eliminado com sucesso.',
                    'type' => 'success',
                    'result' => 'true'
                ];
            }

        } else {
            $result = [
                'feedback' => 'Erro ao eliminar o Equipamento.',
                'type' => 'error',
                'result' => 'true'
            ];
        }

        /* Load to a temporary session feedback */
        $this->session->set_flashdata('result', $result);

        redirect('admin/equipments');
    }

    /* Set slug of Equipment */

    public function datatables()
    {
        /* Get all equipments */
        $equipments = Equipment::get();

        $arr = [];

        /* Load data to a array */
        foreach ($equipments as $key => $value) {
            /* Get images from Equipment*/
            $photo = EquipmentPhoto::where('equipment_id', $value->id)->first();
            if (!empty($photo)) {
                $foto = "<img src='" . base_url() . $photo->fileurl . "' alt='" . $photo->filename . "' class='image-preview' width='50px' max-height='50px'>";
            } else {
                $foto = "<img src='" . base_url() . "assets/img/default.svg' class='image-preview' width='50px'>";
            }

            $row = [];

            $row[] = $value->id;
            $row[] = $foto;
            $row[] = $value->brand . '<br/>' . $value->model;
            $row[] = anchor('admin/equipments/edit/' . $value->id, $value->full_name);

            //category
            $row[] = $value->categories()->pluck('name');
            //status
            if ($value->is_active)
                $row[] = '<a href="' . base_url() .'admin/equipments/deactivate/' . $value->id . '"><span class="label label-success">Activo</span></a>';
            else
                $row[] = '<a href="' . base_url() .'admin/equipments/activate/' . $value->id . '"><span class="label label-danger">Inativo</span></a>';

            $row[] = Carbon\Carbon::parse($value->created_at)->toDateTimeString();
            $row[] = anchor('admin/equipments/edit/' . $value->id, '<i class="fa fa-pencil"></i>', array('class' => 'btn btn-default')) . '<span class="margin-left-5"></span>' .
                '<a href="' . base_url('admin/equipments/delete/') . $value->id . '" class="btn btn-danger">
                            <i class="fa fa-remove"></i></a>';

            $arr[] = $row;
        }

        $output = [
            "data" => $arr,
        ];

        echo json_encode($output);
    }

    /**
     * Activate the specified Equipment in storage.
     *
     * @param  $id
     * @return redirect to view
     */
    function activate($id)
    {
        if (empty($id)) {
            redirect('admin/equipments');
        }

        $activate = Equipment::where('id', $id)->update(['is_active' => 1]);
        $name = Equipment::where('id', $id)->pluck('name');

        /* Check if user is activate */
        if ($activate) {
            $result = [
                'feedback' => '#'.$name . ' activado com sucesso.',
                'type' => 'success',
                'result' => 'true'
            ];
        } else {
            $result = [
                'feedback' => 'Erro ao activar o Equipamento.',
                'type' => 'error',
                'result' => 'true'
            ];
        }

        /* Load to a temporary session feedback */
        $this->session->set_flashdata('result', $result);
        redirect('admin/equipments');
    }

    /**
     * Deactivate the specified Equipment in storage.
     *
     * @param  $id
     * @return redirect to view
     */
    function deactivate($id)
    {
        if (empty($id)) {
            redirect('admin/equipments');
        }

        //Desactivate Equipment by id
        $activate = Equipment::where('id', $id)->update(['is_active' => 0]);
        $name = Equipment::where('id', $id)->pluck('name');

        /* Set feedback */
        if ($activate) {
            $result = [
                'feedback' => '#' . $name . ' desativado com sucesso.',
                'type' => 'success',
                'result' => 'true'
            ];
        } else {
            $result = [
                'feedback' => 'Erro ao desativar o Equipamento.',
                'type' => 'error',
                'result' => 'true'
            ];
        }

        /* Load to a temporary session feedback */
        $this->session->set_flashdata('result', $result);
        redirect('admin/equipments');
    }
}
