<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accessories extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Title Page :: Common */
        $this->page_title->push(lang('word.accessories'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('word.accessories'), 'admin/accessories');

        /* Permission */
        $this->permission = 'acessories';

        /* Verify Permission */
        if (!$this->ion_auth->in_group($this->permission) && !$this->ion_auth->is_admin()) {
            $result = [
                'feedback' => 'Não possui permissão para aceder a esta página.',
                'type'     => 'error',
                'result'   => 'true'
            ];

            /* Return feedback */
            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }
    }

    /**
     * Display a listing of the Accessories.
     * @return template -> render a view
     */
    public function index()
    {
        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Load Template */
        $this->template->admin_render('admin/accessories/index', $this->data);
    }

    /*
     * Load breadcrumbs and page to create Accessory
     * @return Render a view
     */
    public function create()
    {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, 'Adicionar Acessório', 'admin/accessories/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Title of page */
        $this->data['action'] = 'Adicionar Acessório';

        /* Route of form*/
        $this->data['formRoute'] = 'admin/accessories/store';

        /* Get All Categories */
        $this->data['categories'] = Category::all();

        /* Check if exist categories*/
        if ($this->data['categories']->isEmpty()) {
            $result = [
                'feedback' => 'Insira uma categoria primeiro.',
                'type'     => 'error',
                'result'   => 'true'
            ];

            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }

        /* Load Template */
        $this->template->admin_render('admin/accessories/edit', $this->data);
    }

    /*
     * Redirect to update with id null
     */
    public function store()
    {
        return $this->update($id = null);
    }

    /**
     * Update the specified Accessory in storage.
     *
     * @param  $this ->input->post()
     * @param  int $id
     * @return redirect to view
     */
    public function update($id = null)
    {
        //Action to set feedback
        $action = empty($id) ? 'create' : 'edit/' . $id;

        //Get Accessory or Create a new one
        $accessory = Accessory::findOrNew($id);

        //Get Post data
        $input = $this->input->post();

        $categories = $input['categories'];
        unset($input['categories']);

        /* Rules of input */
        $this->form_validation->set_rules('name', 'Nome', 'trim|required|min_length[3]');

        //Only validate input if exist
        if (!empty($input)) {

            // Validate input
            if ($this->form_validation->run() == TRUE) {

                //Mass assignment and save
                $accessory->fill($input);
                $accessory->save();

                //Sync relation
                $accessory->categories()->sync($categories);

                //Set feedback
                if ($action == 'create') {
                    $result = [
                        'feedback' => 'Acessório criado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                } else {
                    $result = [
                        'feedback' => 'Acessório #' . $accessory->name . ' editado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                }

                /* Return feedback */
                $this->session->set_flashdata('result', $result);
                redirect('admin/accessories');
            }
        }

        //On error back to index page with error and post data
        $result = [
            'feedback' => validation_errors(),
            'type' => 'error',
            'result' => 'true'
        ];

        /* Return feedback */
        $this->session->set_flashdata('accessory', $input);
        $this->session->set_flashdata('result', $result);
        redirect('admin/accessories/' . $action);
    }

    /**
     * Load the view to edit Accessory in storage.
     *
     * @param  $id
     * @return Render view
     */
    public function edit($id)
    {
        /* Verify if id came in form */
        if (empty($id)) {
            redirect('admin/accessories');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, 'Editar Acessório', 'admin/accessories/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Get data from accessory */
        $this->data['accessory'] = Accessory::where('id', $id)->first();

        /* If dont exist redirect to list of tips */
        if (!$this->data['accessory']) {
            redirect('admin/accessories');
        }

        /* Title of page */
        $this->data['action'] = 'Editar Acessório';

        /* Route of form*/
        $this->data['formRoute'] = 'admin/accessories/update/' . $this->data['accessory']->id;

        /* Get All Categories */
        $this->data['categories'] = Category::all();

        /* Check */
        if ($this->data['categories']->isEmpty()) {
            $result = [
                'feedback' => 'Insira uma categoria primeiro.',
                'type' => 'error',
                'result' => 'true'
            ];

            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }

        /* Get Select Categories from Accessory*/
        $this->data['selectedCategories'] = $this->data['accessory']->categories()->get();

        /* Load Template */
        $this->template->admin_render('admin/accessories/edit', $this->data);
    }



    /* Loading table data*/

    /**
     * Delete the specified Tip in storage.
     *
     * @param  $id
     * @return redirect to view
     */
    public function delete($id)
    {
        $deleted = Accessory::where('id', $id)->delete();

        if ($deleted) {
            $result = [
                'feedback' => 'Acessório eliminado com sucesso.',
                'type' => 'success',
                'result' => 'true'
            ];
        } else {
            $result = ['feedback' => 'Erro ao eliminar o Acessório.',
                'type' => 'error',
                'result' => 'true'];
        }

        /* Return feedback */
        $this->session->set_flashdata('result', $result);
        redirect('admin/accessories');
    }

    /* Set slug of Tip */

    public
    function datatables()
    {

        /* Get all Accessories */
        $accessories = Accessory::get();

        $arr = [];

        /* Load data to a array */
        foreach ($accessories as $key => $value) {

            $category = "";
            foreach ($value->categories()->select('name')->get() as $categories) {
                $category .= '<span class="label label-default">' . $categories->name . '</span><br/>';
            }

            $row = [];
            $row[] = $value->id;
            $row[] = anchor('admin/accessories/edit/'.$value->id, $value->name);
            $row[] = $category;

            $row[] = Carbon\Carbon::parse($value->created_at)->toDateTimeString();

            $row[] = anchor('admin/accessories/edit/' . $value->id, '<i class="fa fa-pencil"></i>', array('class' => 'btn btn-default')) . '<span class="margin-left-5"></span>' .
                '<a href="' . base_url('admin/accessories/delete/') . $value->id . '" class="btn btn-danger"><i class="fa fa-remove"></i></a>';

            $arr[] = $row;
        }

        $output = [
            "data" => $arr,
        ];

        echo json_encode($output);
    }

}
