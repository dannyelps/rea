<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CatalogEquipments extends Admin_Controller
{


    /**
     * Load the view to edit catalogEquipment in storage.
     *
     * @param  $id
     * @return Render view
     */
    public function store($catalogId, $id)
    {
        /* Verify if id came in form */
        if (empty($catalogId) && empty($id)) {
            redirect('admin/catalogs');
        }

        /* Check catalog */
        $catalog = Catalog::where('id', $catalogId)->first();

        //Sync equipments
        $catalog->equipments()->attach($id);

        redirect('admin/catalogs/edit/'.$catalog->id);
    }

    /**
     * Delete the specified CatalogEquipment in storage.
     *
     * @param  $id
     * @return redirect to view
     */
    public function delete($catalogId, $id)
    {
        /* Verify if id came in form */
        if (empty($catalogId) && empty($id)) {
            redirect('admin/catalogs');
        }

        /* Check catalog */
        $catalog = Catalog::where('id', $catalogId)->first();

        //Sync equipments
        $catalog->equipments()->detach($id);

        redirect('admin/catalogs/edit/'.$catalog->id);
    }

    /* Set datatables of CatalogEquipment */
    public function datatables($id)
    {
        /* Get catalog */
        $catalog = Catalog::where('id', $id)->first();

        /* Get all equipments */
        $equipments = Equipment::all();

        /* Get all equipments from catalog */
        $equipmentsOfCatalog = $catalog->equipments()->get();

        $arr = [];

        /* Load data to a array */
        foreach ($equipments as $key => $value) {

            /* Get one image from Equipment*/
            $photo = EquipmentPhoto::where('equipment_id', $value->id)->first();

            if (!empty($photo)) {
                $foto = "<img src='" . base_url() . $photo->fileurl . "' alt='" . $photo->filename . "' class='image-preview' width='50px' height='50px'>";
            } else {
                $foto = "<img src='" . base_url() ."assets/img/default.svg' class='image-preview' width='50px' height='50px'>";
            }

            $row = [];
            $row[] = $value->id;
            $row[] = $foto;
            $row[] = $value->brand . '<br/>' . $value->model;
            $row[] = $value->name;

            if($equipmentsOfCatalog->contains($value->id )) {
                $row[] =anchor('admin/catalogEquipments/delete/'.$catalog->id . '/'. $value->id, '<span class="btn btn-danger">Remover</span>');
            } else {
                $row[] = anchor('admin/catalogEquipments/store/'.$catalog->id . '/'. $value->id, '<span class="btn btn-success">Adicionar</span>');
            }
            $arr[] = $row;
        }

        $output = [
            "data" => $arr,
        ];

        echo json_encode($output);
    }


}
