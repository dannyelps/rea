<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends Admin_Controller
{
    private static $levelsIcon = [
        'INFO'  => 'fa fa-fw fa-info-circle',
        'ERROR' => 'fa fa-fw fa-exclamation-circle',
        'DEBUG' => 'fa fa-fw fa-exclamation-triangle',
    ];

    private static $levelClasses = [
        'INFO'  => 'info',
        'ERROR' => 'danger',
        'DEBUG' => 'warning',
    ];

    const LOG_LINE_START_PATTERN = "/((INFO)|(ERROR)|(DEBUG)|(ALL))[\s-\d:]+(-->)/";
    const LOG_DATE_PATTERN = "/(\d{4,}-[\d-:]{2,})\s([\d:]{2,})/";
    const LOG_LEVEL_PATTERN = "/^((ERROR)|(INFO)|(DEBUG))/";
    const FILE_PATH_PATTERN = APPPATH . "/logs/log-*.php";
    const LOG_FOLDER_PREFIX = APPPATH . "/logs";
    const MAX_LOG_SIZE = 52428800; //50MB
    const MAX_STRING_LENGTH = 300; //300 chars

    /* based on */
    //https://github.com/SeunMatt/codeigniter-log-viewer/tree/master/src
    public function __construct()
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        }
    }

    /**
     * Default route for rendering view
     *
     * @param String $log_date
     */
    public function index($log_date = NULL)
    {
        /* Title Page */
        $this->page_title->push(lang('log'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Teste */
        $fileName = ($this->input->get("f")) ? $this->input->get("f") : null;

        //get the log files from the log directory
        $files = $this->getFiles();
        if(!is_null($fileName)) {
            $currentFile = self::LOG_FOLDER_PREFIX . "/". base64_decode($fileName);
        }
        else if(is_null($fileName) && !empty($files)) {
            $currentFile = self::LOG_FOLDER_PREFIX. "/" . $files[0];
        }
        else {
            /* if it does not exist initialize to null */
            $this->data['logs'] = [];
            $this->data['files'] = [];
            $this->data['currentFile'] = "";
            return $this->template->admin_render('admin/logs/index', $this->data);
        }

        $logs = $this->processLogs($this->getLogs($currentFile));
        $this->data['logs'] = $logs;
        $this->data['files'] =  $files;
        $this->data['currentFile'] = basename($currentFile);

        /* Set view */
        $this->template->admin_render('admin/logs/index', $this->data);
    }

    /*
     * This will get all the files in the logs folder
     * It will reverse the files fetched and
     * make sure the latest log file is in the first index
     *
     * @param boolean. If true returns the basenames of the files otherwise full path
     * @returns array of file
     * */
    private function getFiles($basename = true)
    {
        $files = glob(self::FILE_PATH_PATTERN);
        $files = array_reverse($files);
        $files = array_filter($files, 'is_file');
        if ($basename && is_array($files)) {
            foreach ($files as $k => $file) {
                $files[$k] = basename($file);
            }
        }
        return array_values($files);
    }

    /*
     * This function will process the logs. Extract the log level, icon class and other information
     * from each line of log and then arrange them in another array that is returned to the view for processing
     *
     * @params logs. The raw logs as read from the log file
     * @return array. An [[], [], [] ...] where each element is a processed log line
     * */
    private function processLogs($logs) {
        if(is_null($logs)) {
            return null;
        }
        $superLog = [];
        foreach ($logs as $log) {
            //get the logLine Start
            $logLineStart = $this->getLogLineStart($log);
            if(!empty($logLineStart)) {
                //this is actually the start of a new log and not just another line from previous log
                $level = $this->getLogLevel($logLineStart);
                $data = [
                    "level" => $level,
                    "date" => $this->getLogDate($logLineStart),
                    "icon" => self::$levelsIcon[$level],
                    "class" => self::$levelClasses[$level],
                ];
                if(strlen($log) > self::MAX_STRING_LENGTH) {
                    $data['content'] = substr($log, 0, self::MAX_STRING_LENGTH);
                    $data["extra"] = substr($log, (self::MAX_STRING_LENGTH + 1));
                } else {
                    $data["content"] = $log;
                }
                array_push($superLog, $data);
            } else if(!empty($superLog)) {
                //this log line is a continuation of previous logline
                //so let's add them as extra
                $prevLog = $superLog[count($superLog) - 1];
                $extra = (array_key_exists("extra", $prevLog)) ? $prevLog["extra"] : "";
                $prevLog["extra"] = $extra . "<br>" . $log;
                $superLog[count($superLog) - 1] = $prevLog;
            } else {
                //this means the file has content that are not logged
                //using log_message()
                //they may be sensitive! so we are just skipping this
                //other we could have just insert them like this
//               array_push($superLog, [
//                   "level" => "INFO",
//                   "date" => "",
//                   "icon" => self::$levelsIcon["INFO"],
//                   "class" => self::$levelClasses["INFO"],
//                   "content" => $log
//               ]);
            }
        }
        return $superLog;
    }

    /*
     * extract the log level from the logLine
     * @param $logLineStart - The single line that is the start of log line.
     * extracted by getLogLineStart()
     *
     * @return log level e.g. ERROR, DEBUG, INFO
     * */
    private function getLogLevel($logLineStart) {
        preg_match(self::LOG_LEVEL_PATTERN, $logLineStart, $matches);
        return $matches[0];
    }
    private function getLogDate($logLineStart) {
        preg_match(self::LOG_DATE_PATTERN, $logLineStart, $matches);
        return $matches[0];
    }
    private function getLogLineStart($logLine) {
        preg_match(self::LOG_LINE_START_PATTERN, $logLine, $matches);
        if(!empty($matches)) {
            return $matches[0];
        }
        return "";
    }

    /*
     * returns an array of the file contents
     * each element in the array is a line in the file
     * @returns array | each line of file contents is an entry in the returned array.
     * @params complete fileName
     * */
    private function getLogs($fileName) {
        $size = filesize($fileName);
        if(!$size || $size > self::MAX_LOG_SIZE)
            return null;
        return file($fileName, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    }

    /*
     * Delete one or more log file in the logs directory
     * @param filename. It can be all - to delete all log files - or specific for a file
     * */
    function deleteFiles($fileName) {
        if($fileName == "all") {
            array_map("unlink", glob(self::FILE_PATH_PATTERN));
            $result = [
                'feedback' => 'Logs eliminados com sucesso.',
                'type'     => 'success',
                'result'   => 'true'
            ];
        }
        else {
            unlink(self::LOG_FOLDER_PREFIX . "/" . $fileName);
            $result = [
                'feedback' => 'Log eliminado com sucesso.',
                'type'     => 'success',
                'result'   => 'true'
            ];
        }

        $this->session->set_flashdata('result', $result);
        redirect('admin/logs', 'refresh');
    }
    /*
     * Download a particular file to local disk
     * @param $fileName
     * */
    function downloadFile($fileName) {
        $file = self::LOG_FOLDER_PREFIX . "/" . $fileName;
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
        }

    }

}
