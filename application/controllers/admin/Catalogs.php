<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogs extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Title Page :: Common */
        $this->page_title->push('Catálogo');
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'Catálogo', 'admin/catalogs');

        /* Permission */
        $this->permission = 'admin';

        /* Verify Permission */
        if (!$this->ion_auth->in_group($this->permission)) {
            $result = [
                'feedback' => 'Não possui permissão para aceder a esta página.',
                'type'     => 'error',
                'result'   => 'true'
            ];

            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }
    }

    /**
     * Display a listing of the equipments.
     *
     * @return template -> render a view
     */
    public function index()
    {
        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Get all categories for modal create */
        $this->data['equipments'] = Equipment::all()->count();

        /* Check if exist categories */
        if ($this->data['equipments'] == 0) {
            $result = [
                'feedback' => 'Insira um equipamento primeiro.',
                'type'     => 'error',
                'result'   => 'true'
            ];

            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }

        /* Load Template */
        $this->template->admin_render('admin/catalogs/index', $this->data);
    }

    /*
     * Redirect to update with id null
     */
    public function store()
    {
        return $this->update($id = null);
    }

    /**
     * Load the view to edit catalog in storage.
     *
     * @param  $id
     * @return Render view
     */
    public function edit($id)
    {
        /* Verify if id came in form */
        if (empty($id)) {
            redirect('admin/catalogs');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, 'Editar Catálogo', 'admin/catalogs/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Get data from catalog */
        $this->data['catalog'] = Catalog::where('id', $id)->first();

        /*Check if exist equipment*/
        if (empty($this->data['catalog'])) {
            redirect('admin/catalogs');
        }

        /* Load Template */
        $this->template->admin_render('admin/catalogs/edit', $this->data);
    }

    /**
     * Update the specified equipament in storage.
     *
     * @param  $this ->input->post()
     * @param  int $id
     * @return redirect to view
     */
    public function update($id = null)
    {
        $action = empty($id) ? '' : 'edit/' . $id;

        //Get equipment or add a new one
        $catalog = Catalog::findOrNew($id);

        //Get Post data
        $input = $this->input->post(['name']);

        /* Rules of input */
        $this->form_validation->set_rules('name', 'Nome', 'trim|required|min_length[3]');

        //Only validate input if exist
        if (!empty($input)) {

            // Validate input
            if ($this->form_validation->run() == TRUE) {

                //Mass assignment and save
                $catalog->fill($input);
                $catalog->save();

                //Set slug of Catalog
                $slug = $this->setSlug($input['name'], $catalog->id);
                Catalog::where('id', $catalog->id)
                    ->update(['slug' => $slug]);

                //Send feedback to index page
                if ($action == '') {
                    $result = [
                        'feedback' => 'Catálogo criado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                    $action .= 'edit/' . $catalog->id;
                } else {
                    $result = [
                        'feedback' => 'Catálogo #' . $catalog->name . ' editado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                    $action = '';
                }

                //Back to index page with result
                $this->session->set_flashdata('result', $result);
                redirect('admin/catalogs/'. $action );
            } else {

                //Set feedback
                $result = [
                    'feedback' => validation_errors(),
                    'type' => 'error',
                    'result' => 'true'
                ];

                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('catalog', $input);
                redirect('admin/catalogs/' . $action);
            }
        }

        //Set feedback
        $result = [
            'feedback' => 'Campos por preencher.',
            'type' => 'error',
            'result' => 'true'
        ];

        /* Return feedback */
        $this->session->set_flashdata('catalog', $input);
        $this->session->set_flashdata('result', $result);
        redirect('admin/catalogs/' . $action);
    }

    /**
     * Delete the specified user in storage.
     *
     * @param  $id
     * @return redirect to view
     */
    public function delete($id)
    {
        /* Remove catalog */
        $deleted = Catalog::where('id', $id)->delete();

        /* Set feedback */
        if ($deleted) {
            $result = [
                'feedback' => 'Catálogo eliminado com sucesso.',
                'type' => 'success',
                'result' => 'true'
            ];
        } else {
            $result = [
                'feedback' => 'Erro ao eliminar o Catálogo.',
                'type' => 'error',
                'result' => 'true'
            ];
        }

        /* Load to a temporary session feedback and redirect */
        $this->session->set_flashdata('result', $result);
        redirect('admin/catalogs');
    }

    /* Set datatables of catalogs */
    public function datatables()
    {
        /* Get all catalogs */
        $catalogs = Catalog::get();

        $arr = [];

        /* Load data to a array */
        foreach ($catalogs as $key => $value) {
            $row = [];
            $row[] = $value->id;
            $row[] = anchor('admin/catalogs/edit/' . $value->id, $value->name);
            $row[] = $value->equipments()->count();
            $row[] = Carbon\Carbon::parse($value->created_at)->toDateTimeString();
            $row[] = anchor('admin/catalogs/edit/' . $value->id, '<i class="fa fa-pencil"></i>', array('class' => 'btn btn-default')) . '<span class="margin-left-5"></span>' .
                '<a href="' . base_url('admin/catalogs/delete/') . $value->id . '" class="btn btn-danger">
                            <i class="fa fa-remove"></i></a>';

            $arr[] = $row;
        }

        $output = [
            "data" => $arr,
        ];

        echo json_encode($output);
    }

    /* Set Slug */
    public function setSlug($name, $id)
    {
        $accents = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'þ', 'ÿ');
        $noAccents = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'B', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'p', 'y');

        $name = str_replace($accents, $noAccents, $name);
        $name = strtolower($name);

        $slug = str_slug($name, "-");
        $params = array();
        $params['slug'] = $slug;
        $i = 0;

        /* Check if exist and increase number */
        while (Catalog::where('id', '!=', $id)->where('slug', $slug)->first()) {
            if (!preg_match('/-{1}[0-9]+$/', $slug)) {
                $slug .= '-' . ++$i;
            } else {
                $slug = preg_replace('/[0-9]+$/', ++$i, $slug);
            }
            $params ['slug'] = $slug;
        }

        /* Return the final slug */
        return $slug;
    }
}
