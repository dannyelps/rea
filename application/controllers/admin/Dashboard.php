<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in() OR !$this->ion_auth->is_admin()) {
            redirect('auth/login', 'refresh');
        }
    }


    public function index()
    {
        /* Title Page */
        $this->page_title->push(lang('menu_dashboard'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        $this->data['count_users'] = User::get()->count();
        $this->data['count_equipment'] = Equipment::IsActive()->get()->count();
        $this->data['count_reports'] = Report::get()->count();

        $this->data['reports'] = Report::IsNotRead()->get();

        /* Only get if user is dev */
        if($this->ion_auth->is_dev()) {
            $this->data['logins'] = User::select('name', 'last_login')
                ->whereNotNull('last_login')
                ->take(10)
                ->orderby('last_login', 'desc')
                ->get();
        }

        Carbon\Carbon::setLocale('pt');

        /* Load Template */
        $this->template->admin_render('admin/dashboard/index', $this->data);
    }
}
