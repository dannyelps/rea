<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EquipmentsPhotos extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('image_lib');

        /* Permission */
        $this->permission = 'equipments';

        /* Verify Permission */
        if (!$this->ion_auth->is_admin()) {
            $result = [
                'feedback' => 'Não possui permissão para aceder a esta página.',
                'type'     => 'error',
                'result'   => 'true'
            ];

            /* Return feedback */
            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }
    }

    /**
     * Store a newly created resource in storage.
     * POST /admin/EquipmentsPhotos/store/{$equipmentId}
     *
     * @return Response
     */
    public function store($equipmentId)
    {
        /* Get Equipment data */
        $equipment = Equipment::findOrFail($equipmentId);
        if (!$equipment)
            redirect('admin/equipments', 'refresh');

        /* Get File data */
        $input = $_FILES['file'];

        /* Create a new Equipment Photo Model */
        $equipmentPhoto = new EquipmentPhoto();
        $equipmentPhoto->equipment_id = $equipment->id;

        /* Upload File*/
        if (!$equipmentPhoto->upload($input)) {
            $this->output->set_status_header('400');
        } else {
            //Set status of header
            $this->output->set_status_header('200');

            //Send photo data to view
//            $this->data['newPhotos'] = EquipmentPhoto::where('id', $equipmentPhoto->id)
//                                                        ->get();

        }
    }

    /* Remove a photo from a Equipment */
    public function destroy($equipment_id, $id) {

        /* Get data from photo or fail */
        $photo = EquipmentPhoto::findOrFail($id);

        /* Check if exists error on delete */
        if (!$photo->delete()) {
            $result = [
                'feedback' => 'Erro ao remover a imagem.',
                'type'     => 'error',
                'result'   => 'true'
            ];

            /* Return feedback of error */
            $this->session->set_flashdata('result', $result);
            redirect('admin/tips/edit/'. $equipment_id);
        } else {
//            /* Remove image from folder */
//            unlink("uploads/".$photo->fileurl);
        }
        $result = [
            'feedback' => 'Imagem removida com sucesso.',
            'type'     => 'success',
            'result'   => 'true'
        ];

        /* Return feedback of error */
        $this->session->set_flashdata('result', $result);
        redirect('admin/equipments/edit/'. $equipment_id .'#tab-fotos');
    }

    /**
     * Remove all selected resources from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function massDestroy($equipment_id) {

        /* Remove all Photos */
        $result = EquipmentPhoto::where('equipment_id', $equipment_id)->delete();

        if (!$result) {
           return false;
        }

        return true;
    }
}
