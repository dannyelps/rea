<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TipsPhotos extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('image_lib');

        /* Title Page :: Common */
        $this->page_title->push(lang('word.tips'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('word.tips'), 'admin/tips');

        /* Permission */
        $this->permission = 'admin';

        /* Verify Permission */
        if (!$this->ion_auth->in_group($this->permission) && !$this->ion_auth->is_admin()) {
            $result = [
                'feedback' => 'Não possui permissão para aceder a esta página.',
                'type' => 'error',
                'result' => 'true'
            ];

            /* Return feedback */
            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }
    }

    /**
     * Store a newly created resource in storage.
     * POST /admin/tips/{tipId}/fotos
     *
     * @return Response
     */
    public function store($tipId)
    {
        /* Get Tip data */
        $tip = Tip::findOrFail($tipId);
        if (!$tip)
            redirect('admin/tips', 'refresh');

        /* Get File data */
        $input = $_FILES['file'];

        /* Create a new Tip Photo Model */
        $tipPhoto = new TipPhoto;
        $tipPhoto->tips_id = $tip->id;

        /* Upload File*/
        if (!$tipPhoto->upload($input)) {
            $this->output->set_status_header('400');
        } else {
            //Set status of header
            $this->output->set_status_header('200');

//            //
//            $this->data['photos'] = TipPhoto::where('tips_id', $tipPhoto->tips_id)->get();

        }
    }

    /* Remove a photo from a Tip */
    public function destroy($tip_id, $id) {

        /* Get data from photo or fail */
        $photo = TipPhoto::findOrFail($id);

        /* Check if exists error on delete */
        if (!$photo->delete()) {
            $result = [
                'feedback' => 'Erro ao remover a imagem.',
                'type'     => 'error',
                'result'   => 'true'
            ];

            /* Return feedback of error */
            $this->session->set_flashdata('result', $result);
            redirect('admin/tips/edit/'. $tip_id);
        } else {
            /* Remove image from folder */
//            unlink("uploads/".$photo->fileurl);
        }
        $result = [
            'feedback' => 'Imagem removida com sucesso.',
            'type'     => 'success',
            'result'   => 'true'
        ];

        /* Return feedback of error */
        $this->session->set_flashdata('result', $result);
        redirect('admin/tips/edit/'. $tip_id .'#tab-fotos');
    }

    /**
     * Remove all selected resources from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function massDestroy($tipId) {

        $result = TipPhoto::where('tips_id', $tipId)->delete();

        if (!$result) {
           return false;
        }

        return true;
    }
}
