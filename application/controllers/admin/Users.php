<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Load :: Language File */
        $this->lang->load('admin/users');

        /* Title Page :: Common */
        $this->page_title->push(lang('word.users'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('word.users'), 'admin/users');

        /*
         * Permission
         *
         * # Permission required to open controller
        */
        $this->permission = 'admin';


        /*
         * Verify Permission
         */
        if (!$this->ion_auth->in_group($this->permission)) {
            $result = [
                'feedback' => 'Não possui permissão para aceder a esta página.',
                'type' => 'error',
                'result' => 'true'
            ];

            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');

        }
    }

    /**
     * Display a listing of the users.
     *
     * @return template -> render a view
     */
    public function index()
    {
        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Load Template */
        $this->template->admin_render('admin/users/index', $this->data);
    }

    /*
     * Load breadcrumbs and page to create user
     *
     * @return Render a view
     */
    public function create()
    {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('user_create'), 'admin/users/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Load Template */
        $this->template->admin_render('admin/users/create', $this->data);
    }

    /*
     * Redirect to update with id null
     */
    public function store()
    {
        return $this->update($id = null);
    }

    /**
     * Update the specified user in storage.
     *
     * @param  $this ->input->post()
     * @param  int $id
     * @return redirect to view
     */
    public function update($id = null)
    {
        $action = empty($id) ? 'create' : 'edit/' . $id;

        //Get user or Create a new one
        $user = User::findOrNew($id);

        //Get Post data
        $input = $this->input->post();

        $permissions = $input['permissions'];
        unset($input['permissions']);

        /* Rules of input */
        $this->form_validation->set_rules('name', 'Nome', 'trim|required');

        //Set validation on create
        if ($action == 'create') {
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('password', 'password', 'trim|required|matches[check_password]');
            $this->form_validation->set_rules('check_password', 'Confirmar Password', 'trim|required');
        }

        //Only validate input if exist
        if (!empty($input)) {

            // Validate input
            if ($this->form_validation->run() == TRUE) {

                //if don't exist unset
                if (empty($input['password'])) {
                    unset($input['password']);
                    unset($input['check_password']);
                }

                //Set IP of Client
                $input['ip_address'] = $this->input->ip_address();

                //Mass assignment and save
                $user->fill($input);
                $user->save();

                //Sync relation
                $user->permission()->sync($permissions);

                //Send feedback to index page
                if ($action == 'create') {
                    $result = [
                        'feedback' => 'Utilzador criado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                } else {
                    $result = [
                        'feedback' => 'Utilzador #' . $user->name . ' editado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                }


                //Back to index page with result
                $this->session->set_flashdata('result', $result);
                redirect('admin/users');
            } else {

                //On error back to index page with error and post data
                $result = [
                    'feedback' => validation_errors(),
                    'type' => 'error',
                    'result' => 'true'
                ];

                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('user', $input);
                redirect('admin/users/' . $action);
            }
        }

        //On error back to index page with error and post data
        $result = [
            'feedback' => 'Campos por preencher.',
            'type' => 'error',
            'result' => 'true'
        ];

        $this->session->set_flashdata('user', $input);
        $this->session->set_flashdata('result', $result);
        redirect('admin/users/' . $action);
    }

    /**
     * Load the view to edit user in storage.
     *
     * @param  $id
     * @return Render view
     */
    public function edit($id)
    {
        /* Verify if id came in form */
        if (empty($id)) {
            redirect('admin/users');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('user_edit'), 'admin/users/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Get data from user */
        $this->data['user'] = User::where('id', $id)->first();

        /* If dont exist redirect to list of users */
        if (!$this->data['user']) {
            redirect('admin/users');
        }

        $this->data['currentGroups'] = $this->ion_auth->get_users_groups($id)->result();

        /* Only devs set permissions and edit */
        if($this->ion_auth->is_dev()) {
            /* Get all Permissions*/
            $this->data['permissions'] = Permission::all();
            $this->data['permissions_user'] = $this->data['user']->permission()->get();
        }

        // display the edit user form
        $this->data['_token'] = $this->_get_csrf_nonce();

        /* Load Template */
        $this->template->admin_render('admin/users/edit', $this->data);
    }



    /**
     * Delete the specified user in storage.
     *
     * @param  $id
     * @return redirect to view
     */
    public function delete($id)
    {

        $deleted = User::where('id', $id)->delete();

        /* Check if user is logged in */
        if ($id == $this->ion_auth->user()->row()->id)
            $this->ion_auth->logout();

        if ($deleted) {
            $result = [
                'feedback' => 'Utilizador eliminado com sucesso.',
                'type' => 'success',
                'result' => 'true'
            ];
        } else {
            $result = [
                'feedback' => 'Erro ao eliminar o Utilizador.',
                'type' => 'error',
                'result' => 'true'
            ];
        }

        /* Load to a temporary session feedback */
        $this->session->set_flashdata('result', $result);

        redirect('admin/users');
    }

    public function datatables()
    {

        /* Get all users */
        $users = User::get();

        $arr = [];

        /* Load data to a array */
        foreach ($users as $key => $value) {
            $row = [];
            $row[] = $value->id;
            $row[] = anchor('admin/users/edit/' . $value->id, $value->name);
            $row[] = $value->email;

            //change status of user
            if ($value->is_active)
                if($this->ion_auth->is_dev())
                    $row[] = '<a href="' . base_url() .'admin/users/deactivate/' . $value->id . '"><span class="label label-success">Activo</span></a><br/><span class="text-muted">'.$value->ip_address.'</span>';
                else
                    $row[] = '<a href="' . base_url() .'admin/users/deactivate/' . $value->id . '"><span class="label label-success">Activo</span></a>';
            else
                if($this->ion_auth->is_dev())
                    $row[] = '<a href="' . base_url() .'admin/users/activate/' . $value->id . '"><span class="label label-danger">Inativo</span></a><br/><span class="text-muted">'.$value->ip_address.'</span>';
                else
                    $row[] = '<a href="' . base_url() .'admin/users/activate/' . $value->id . '"><span class="label label-danger">Inativo</span></a>';

            $row[] = Carbon\Carbon::parse($value->created_at)->toDateTimeString();
            $row[] = anchor('admin/users/profile/' . $value->id, '<i class="fa fa-eye"></i>', array('class' => 'btn btn-default')) . '<span class="margin-left-5"></span>' .
                anchor('admin/users/edit/' . $value->id, '<i class="fa fa-pencil"></i>', array('class' => 'btn btn-default')) . '<span class="margin-left-5"></span>' .
                '<a href="' . base_url('admin/users/delete/') . $value->id . '" data-method="delete" data-confirm="Confirma a remoção do registo selecionado?" class="btn btn-danger">
                            <i class="fa fa-remove"></i></a>';

            $arr[] = $row;
        }

        $output = [
            "data" => $arr,
        ];

        echo json_encode($output);
    }

    /**
     * Activate the specified user in storage.
     *
     * @param  $id
     * @return redirect to view
     */
    function activate($id)
    {
        if (empty($id)) {
            redirect('admin/users');
        }

        $activate = User::where('id', $id)->update(['is_active' => 1]);

        /* Check if user is activate */
        if ($activate) {
            $result = [
                'feedback' => 'Utilizador activado com sucesso.',
                'type' => 'success',
                'result' => 'true'
            ];
        } else {
            $result = [
                'feedback' => 'Erro ao activar o Utilizador.',
                'type' => 'error',
                'result' => 'true'
            ];
        }

        /* Load to a temporary session feedback */
        $this->session->set_flashdata('result', $result);
        redirect('admin/users');
    }

    /**
     * Deactivate the specified user in storage.
     *
     * @param  $id
     * @return redirect to view
     */
    function deactivate($id)
    {
        if (empty($id)) {
            redirect('admin/users');
        }

        //Desactivate user by id
        $activate = User::where('id', $id)->update(['is_active' => 0]);


        /* Check if user is deactivate */
        if ($activate) {
            if ($id == $this->ion_auth->user()->row()->id)
                $this->ion_auth->logout();

            $result = [
                'feedback' => 'Utilizador desactivado com sucesso.',
                'type' => 'success',
                'result' => 'true'
            ];
        } else {
            $result = [
                'feedback' => 'Erro ao desactivar o Utilizador.',
                'type' => 'error',
                'result' => 'true'
            ];
        }

        /* Load to a temporary session feedback */
        $this->session->set_flashdata('result', $result);
        redirect('admin/users');
    }

    public function profile($id)
    {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_profile'), 'admin/users/profile');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        $this->data['user'] = User::where('id', $id)->first();
        $this->data['user_permissions'] = '';

        /* Load Template */
        $this->template->admin_render('admin/users/profile', $this->data);
    }

    public function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE && $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

}
