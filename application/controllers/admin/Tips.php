<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tips extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->lang->load('admin/tips');

        /* Title Page :: Common */
        $this->page_title->push(lang('word.tips'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('word.tips'), 'admin/tips');

        /* Permission */
        $this->permission = 'tips';

        /* Verify Permission */
        if (!$this->ion_auth->in_group($this->permission) && !$this->ion_auth->is_admin()) {
            $result = [
                'feedback' => 'Não possui permissão para aceder a esta página.',
                'type' => 'error',
                'result' => 'true'
            ];

            /* Return feedback */
            $this->session->set_flashdata('result', $result);
            redirect('admin', 'refresh');
        }
    }

    /**
     * Display a listing of the tips.
     * @return template -> render a view
     */
    public function index()
    {
        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Load Template */
        $this->template->admin_render('admin/tips/index', $this->data);
    }

    /*
     * Load breadcrumbs and page to create Tip
     * @return Render a view
     */
    public function create()
    {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, 'Adicionar Dica', 'admin/tips/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Load Template */
        $this->template->admin_render('admin/tips/create', $this->data);
    }

    /*
     * Redirect to update with id null
     */
    public function store()
    {
        return $this->update($id = null);
    }

    /**
     * Load the view to edit Tip in storage.
     *
     * @param  $id
     * @return Render view
     */
    public function edit($id)
    {
        /* Verify if id came in form */
        if (empty($id)) {
            redirect('admin/tips');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, 'Editar Dica', 'admin/tips/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Get data from Tip */
        $this->data['tip'] = Tip::where('id', $id)->first();

        /* If dont exist redirect to list of tips */
        if (!$this->data['tip']) {
            redirect('admin/tips');
        }

        /* Get catalogs */
        $this->data['catalogs'] = Catalog::all();

        /* Get images from Tip*/
        $this->data['photos'] = TipPhoto::where('tips_id', $this->data['tip']->id)->get();

        /* Load Template */
        $this->template->admin_render('admin/tips/edit', $this->data);
    }

    /**
     * Update the specified Tip in storage.
     *
     * @param  $this ->input->post()
     * @param  int $id
     * @return redirect to view
     */
    public function update($id = null)
    {
        $action = empty($id) ? 'create' : 'edit/' . $id;

        //Get Tip or Create a new one
        $tip = Tip::findOrNew($id);

        //Get Post data
        $input = $this->input->post();

        /* Rules of input */
        $this->form_validation->set_rules('name', 'Nome', 'trim|required');
        $this->form_validation->set_rules('description', 'Descrição', 'trim|required');
        $this->form_validation->set_rules('text', 'Conteúdo', 'trim');

        //Only validate input if exist
        if (!empty($input)) {

            // Validate input
            if ($this->form_validation->run() == TRUE) {

                /* Set catalog as NULL */
                if($input['catalog_id'] == 0) {
                    $input['catalog_id'] = NULL;
                }

                //Mass assignment and save
                $tip->fill($input);
                $tip->save();

                //Set slug of Tip
                $slug = $this->setSlug($input['name'], $tip->id);
                Tip::where('id', $tip->id)
                    ->update(['slug' => $slug]);

                //Set feedback
                if ($action == 'create') {
                    $result = [
                        'feedback' => 'Dica criado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                } else {
                    $result = [
                        'feedback' => 'Dica #' . $tip->name . ' editado com sucesso.',
                        'type' => 'success',
                        'result' => 'true'
                    ];
                }

                /* Return feedback */
                $this->session->set_flashdata('result', $result);
                redirect('admin/tips');
            }
        }

        //On error back to index page with error and post data
        $result = [
            'feedback' => validation_errors(),
            'type' => 'error',
            'result' => 'true'
        ];

        /* Return feedback */
        $this->session->set_flashdata('tip', $input);
        $this->session->set_flashdata('result', $result);
        redirect('admin/tips/' . $action);
    }




    /**
     * Delete the specified Tip in storage.
     *
     * @param  $id
     * @return redirect to view
     */
    public function delete($id)
    {
        $deleted = Tip::where('id', $id)->delete();

        if ($deleted) {

            /* Mass destroy of tips images */
            $photos = TipPhoto::where('tips_id', $id)->get();

            foreach ($photos as $photo) {
//                /* Remove image from folder */
//                unlink($photo->fileurl);
            }

            /* Remove from DB */
            $deleted_images = TipPhoto::where('tips_id', $id)->delete();

            /* Set feedback */
            if (!$deleted_images) {
                $result = [
                    'feedback' => 'Erro ao eliminar imagens da dica.',
                    'type' => 'error',
                    'result' => 'true'
                ];
            } else {
                $result = [
                    'feedback' => 'Dica eliminada com sucesso.',
                    'type' => 'success',
                    'result' => 'true'
                ];
            }
        } else {
            $result = [
                'feedback' => 'Erro ao eliminar a Dica.',
                'type' => 'error',
                'result' => 'true'
            ];
        }

        /* Return feedback */
        $this->session->set_flashdata('result', $result);
        redirect('admin/tips');
    }


    /* Loading table data*/
    public function datatables() {

        /* Get all tips */
        $tips = Tip::get();

        $arr = [];

        /* Load data to a array */
        foreach ($tips as $key => $value) {
            $row = [];
            $row[] = $value->id;
            $row[] = anchor('admin/tips/edit/' . $value->id, $value->name);
            $row[] = str_limit($value->description, 250, '..');

            //How img have
            $row[] = TipPhoto::where('tips_id', $value->id)->count();
            $row[] = Carbon\Carbon::parse($value->created_at)->toDateTimeString();
            $row[] = anchor('admin/tips/edit/' . $value->id, '<i class="fa fa-pencil"></i>', array('class' => 'btn btn-default')) . '<span class="margin-left-5"></span>' .
                '<a href="' . base_url('admin/tips/delete/') . $value->id . '" class="btn btn-danger"><i class="fa fa-remove"></i></a>';

            $arr[] = $row;
        }

        $output = [
            "data" => $arr,
        ];

        echo json_encode($output);
    }

    /* Set slug of Tip */
    public function setSlug($name, $id)
    {
        $accents = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'þ', 'ÿ');
        $noAccents = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'B', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'p', 'y');

        $name = str_replace($accents, $noAccents, $name);
        $name = strtolower($name);

        $slug = str_slug($name, "-");
        $params = array();
        $params['slug'] = $slug;
        $i = 0;

        /* Check if exist and increase number */
        while (Tip::where('id', '!=', $id)->where('slug', $slug)->first()) {
            if (!preg_match('/-{1}[0-9]+$/', $slug)) {
                $slug .= '-' . ++$i;
            } else {
                $slug = preg_replace('/[0-9]+$/', ++$i, $slug);
            }
            $params ['slug'] = $slug;
        }

        /* Return the final slug */
        return $slug;
    }
}
