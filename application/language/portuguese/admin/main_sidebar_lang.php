<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');


$lang['menu_online'] = 'Online';
$lang['menu_search'] = 'Pesquisa';
$lang['create']      = 'Criar';
$lang['edit']        = 'Editar';
$lang['remove']      = 'Remover';

/*
 * Dev
 */
$lang['development'] = 'Desenvolvedor';

/* * */$lang['menu_access_website']             = 'Acesso ao site';
/* * */$lang['menu_back_home']                  = 'Voltar a homepage';

/* * */$lang['menu_main_navigation']            = 'Navegação Principal';
/* ****** */$lang['menu_dashboard']             = 'Painel de instrumentos';


/* * */$lang['menu_administration']             = 'Administração';
/* ****** */$lang['menu_users']                 = 'Utilizadores';
/* *********** */$lang['menu_users_profile']    = 'Perfil';
/* *********** */$lang['menu_users_deactivate'] = 'Desativar';
/* *********** */$lang['menu_users_create']     = 'Criar';
/* *********** */$lang['menu_users_edit']       = 'Editar';

/* ****** */$lang['menu_groups']       = 'Permissões';

/* ****** */$lang['menu_preferences']           = 'Preferências';
/* *********** */$lang['menu_interfaces']       = 'Interfaces';
/* **************** */$lang['menu_int_admin']   = 'Administração';
/* **************** */$lang['menu_int_public']  = 'Público';

/* ****** */$lang['menu_database_utility']      = 'Base de Dados';


/* * */$lang['menu_webapp']                     = 'Aplicação web';
/* ****** */$lang['menu_license']               = 'Licença';
/* ****** */$lang['menu_resources']             = 'Recursos';
