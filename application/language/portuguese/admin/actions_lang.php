<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['cancel']         = 'Cancelar';
$lang['create']         = 'Criar';
$lang['default_values'] = 'Valores padrões';
$lang['edit']           = 'Editar';
$lang['ok']             = 'Ok';
$lang['no']             = 'Não';
$lang['reset']          = 'Reiniciar';
$lang['see']            = 'Ver';
$lang['submit']         = 'Enviar';
$lang['yes']            = 'Sim';
$lang['delete']         = 'Excluir';

$lang['security_error']     = 'Erro de segurança';

