<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['word.action']              = 'Ação';
$lang['word.active']              = 'Ativo';
$lang['word.submit']              = 'Submeter';

$lang['word.new']                 = 'Novo';
$lang['word.edit']                = 'Editar';
$lang['word.remove']              = 'Remover';
$lang['word.insert']              = 'Inserir';
$lang['word.create']              = 'Criar';
$lang['word.reset']               = 'Resetar';
$lang['word.cancel']              = 'Cancelar';
$lang['word.save']                = 'Gravar';
$lang['word.action']              = 'Ação';
$lang['word.actions']             = 'Ações';
$lang['word.status']              = 'Estado';
$lang['word.created_at']          = 'Criado em';


$lang['word.user']                = 'Utilizador';
$lang['word.users']               = 'Utilizadores';

$lang['word.email']               = 'Email';
$lang['word.tips']                = 'Dicas';

$lang['word.accessories']         = 'Acessórios';

$lang['word.categories']          = 'Categorias';