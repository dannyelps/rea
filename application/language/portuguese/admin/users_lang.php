<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['users_action']              = 'Ação';
$lang['users_active']              = 'Ativo';
$lang['users_company']             = 'Companhia';
$lang['user_create']               = 'Criar Utilizador';
$lang['users_created_on']          = 'Criado em';
$lang['users_deactivate_question'] = 'Tem certeza que deseja desativar o usuário %s';
$lang['user_edit']                 = 'Editar Utilizador';
$lang['users_email']               = 'E-mail';
$lang['users_firstname']           = 'Primeiro nome';
$lang['users_groups']              = 'Grupos';
$lang['users_inactive']            = 'Inativo';
$lang['users_ip_address']          = 'Endereço de IP';
$lang['users_last_login']          = 'Último login';
$lang['users_lastname']            = 'Último nome';
$lang['user_groups']               = 'Grupos do Utilizador';
$lang['users_password']            = 'Senha';
$lang['users_password_confirm']    = 'Confirme a senha';
$lang['users_phone']               = 'Telefone';
$lang['users_status']              = 'Condição';
$lang['users_username']            = 'Nome de usuário / Pseudo';
$lang['users_permissions']         = 'Permissões';
$lang['users_list']                = 'Listagem dos utilizadores';



$lang['user_create'] = 'Criar Utilizador';
