<?php if (@$this->session->tempdata('notAllowed')) {
    echo
    '<section class="error-content" id="alert">
        <div class="row">
            <div class="col-xs-offset-4 col-xs-4">
                 <div class="alert alert-danger text-center" role="alert"><i class="fa fa-fw fa-ban"></i>Não têm permissão para aceder.</div>
            </div>
        </div>
    </section>';
} ?>