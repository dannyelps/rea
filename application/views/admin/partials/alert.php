<script>
    $(document).ready(function(){
    if (<?php Session::has('sucess') ?>)
        $.bootstrapGrowl("<i class='fa fa-check'></i> <?php Session::get('success') ?> }}", {type: 'success', align: 'center', width: 'auto', delay: 8000});

    if (<?php Session::has('error') ?>)
        $.bootstrapGrowl("<i class='fa fa-exclamation-circle'></i> <?php Session::get('error') ?>", {type: 'error', align: 'center', width: 'auto', delay: 8000});

    if (<?php Session::has('warning') ?>)
        $.bootstrapGrowl("<i class='fa fa-warning'></i> <?php Session::get('warning') ?>", {type: 'warning', align: 'center', width: 'auto', delay: 8000});

    if (<?php Session::has('info') ?>)
       $.bootstrapGrowl("<i class='fa fa-info-circle'></i> <?php Session::get('info') ?>", {type: 'info', align: 'center', width: 'auto', delay: 8000});
    })
</script>