<style>
    a.llv-active {
        z-index: 2;
        background-color: #f5f5f5;
        border-color: #777;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <?= $pagetitle; ?>
        <?= $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box no-border">
                    <div class="row">
                        <div class="box-body">
                            <div class="col-sm-4 col-md-2">
                                <h2>Ficheiros</h2>
                                <div class="list-group">
                                    <?php if (empty($files)): ?>
                                        <a class="list-group-item liv-active">Não existem logs</a>
                                    <?php else: ?>
                                        <?php foreach ($files as $file): ?>
                                            <a href="?f=<?= base64_encode($file); ?>"
                                               class="list-group-item <?= ($currentFile == $file) ? "llv-active" : "" ?>">
                                                <?= $file; ?>
                                            </a>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                                <?php if (count($files) > 1): ?>
                                    <?= anchor('admin/logs/deleteFiles/all', '<i class="fa fa-fw fa-trash"></i> Apagar todos os Logs', array('class' => 'btn btn-danger width-100')); ?>
                                <?php endif; ?>
                                <div class="spacer-30"></div>
                            </div>
                            <div class="col-sm-8 col-md-10 table-container">
                                <?php if (is_null($logs)): ?>
                                    <div>
                                        Log com tamanho superior a >50M, por favor transfira.
                                    </div>
                                <?php else: ?>
                                    <table id="table-log" class="table table-striped width-100">
                                        <thead>
                                        <tr>
                                            <th class="width-90px">Tipo</th>
                                            <th>Data</th>
                                            <th>Conteúdo</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($logs as $key => $log): ?>
                                            <tr data-display="stack<?= $key; ?>">
                                                <td class="text-<?= $log['class']; ?>">
                                                    <span class="<?= $log['icon']; ?>" aria-hidden="true"></span>
                                                    &nbsp;<?= $log['level']; ?>
                                                </td>
                                                <td class="date"><?= $log['date']; ?></td>
                                                <td class="text">
                                                    <?php if (array_key_exists("extra", $log)): ?>
                                                        <a class="pull-right expand btn btn-default btn-xs"
                                                           data-display="stack<?= $key; ?>">
                                                            <span class="glyphicon glyphicon-search"></span>
                                                        </a>
                                                    <?php endif; ?>
                                                    <?= $log['content']; ?>
                                                    <?php if (array_key_exists("extra", $log)): ?>
                                                        <div class="stack" id="stack<?= $key; ?>"
                                                             style="display: none; white-space: pre-wrap;">
                                                            <?= $log['extra'] ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                <?php endif; ?>
                                <div>
                                    <div class="spacer-10"></div>
                                    <?php if ($currentFile): ?>
                                        <div class="btn-group" role="group" aria-label="example">
                                            <?= anchor('admin/logs/downloadFile/' . $currentFile, '<i class="fa fa-fw fa-download"></i> Download Log', array('class' => 'btn btn-primary')); ?>
                                            <?= anchor('admin/logs/deleteFiles/' . $currentFile, '<i class="fa fa-fw fa-trash"></i> Apagar Log', array('class' => 'btn btn-danger')); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('.table-container tr').on('click', function () {
                    $('#' + $(this).data('display')).toggle();
                });

                $('#table-log').DataTable({
                    "order": [1, 'desc'],
                    "stateSave": true,
                    "stateSaveCallback": function (settings, data) {
                        window.localStorage.setItem("datatable", JSON.stringify(data));
                    },
                    "stateLoadCallback": function (settings) {
                        var data = JSON.parse(window.localStorage.getItem("datatable"));
                        if (data) data.start = 0;
                        return data;
                    }
                });

                $('#delete-log, #delete-all-log').click(function () {
                    return confirm('Têm a certeza que pretende eliminar?');
                });
            });
        </script>
    </section>
</div>





