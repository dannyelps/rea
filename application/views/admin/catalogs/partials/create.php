<div id="create" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Criar Catálogo</h3>
            </div>
            <?= form_open('admin/catalogs/store', array('class' => 'form-group')); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group is-required">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="name" value="<?= @$equipment['full_name'] ?>" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?= form_button(array('type' => 'submit', 'class' => 'btn btn-primary pull-left', 'content' => lang('word.save'))); ?>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
</div>

