<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
    <section class="content-header">
        <?= $pagetitle; ?>
        <?= $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box no-border">
                    <div class="box-header">
                        <h3 class="box-title"> Editar Problema</h3>
                    </div>
                    <?= form_open('admin/catalogs/update/'.$catalog['id'], array('class' => 'form-group')); ?>
                    <div class="box-body">
                        <div class="form-group is-required">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="name" value="<?= @$catalog['name'] ?>" required>
                        </div>
                        <table class="table table-striped width-100" id="datatables">
                            <thead>
                            <tr>
                                <th style="width: 1%">#</th>
                                <th class="width-50px">Imagem</th>
                                <th class="width-70px">Marca<br/>
                                    <small>Modelo</small>
                                </th>
                                <th>Nome</th>
                                <th class="width-80px">Estado</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <?= form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('word.save'))); ?>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function () {

        var oTable = $('#datatables');

        oTable.DataTable({
            // "processing": true,
            // "serverSide": true,
            "ajax": {
                url: "<?= site_url() . 'admin/catalogEquipments/datatables/' . $catalog['id'] ?>",
                type: "POST",
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>'
                },
                dataSrc: function ( json ) {
                    if(json.csrf_token !== undefined) $('meta[name=_token]').attr("content", json.csrf_token);
                    return json.data;
                },
                error: function () {  // error handling
                    oTable.html("");
                    oTable.width('100%');
                    oTable.append('<tbody><tr><th><i class="fa fa-warning text-red" style="margin-right: 5px;"></i><?= lang('error_datatables_loading_data') ?></th></tr></tbody>');
                },
            },
            "columns": [
                {"data": "0"},
                {"data": "1"},
                {"data": "2"},
                {"data": "3", class: 'text-justify'},
                {"data": "4"}
            ]
        });
    });
</script>

