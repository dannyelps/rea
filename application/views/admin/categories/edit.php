<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>


<div class="content-wrapper">
    <section class="content-header">
        <?= $pagetitle; ?>
        <?= $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box no-border">
                    <div class="box-header">
                        <h3 class="box-title"> <?= $action ?></h3>
                    </div>
                    <?= form_open($formRoute, array('class' => 'form-group')); ?>
                    <div class="box-body">
                        <div class="form-group is-required">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="name" value="<?= @$category['name'] ?>" required>
                        </div>
                    </div>
                    <div class="box-footer">
                        <?= form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('word.save'))); ?>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
