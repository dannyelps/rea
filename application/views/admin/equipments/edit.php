<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!-- Plugin need -->
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/icheck/css/blue.css'); ?>">
<script src="<?php echo base_url($plugins_dir . '/icheck/js/icheck.min.js'); ?>"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
    });
</script>

<div class="content-wrapper">
    <section class="content-header">
        <?= $pagetitle; ?>
        <?= $breadcrumb; ?>

    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box no-border">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">
                            <li role="navigation">
                                <a href="#tab-fotos" data-toggle="tab" aria-expanded="true">Fotografias</a>
                            </li>
                            <li role="navigation" class="active">
                                <a href="#tab-info" data-toggle="tab" aria-expanded="false">Detalhes</a>
                            </li>

                            <li class="pull-left header">
                                Editar Equipamento
                            </li>
                        </ul>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-info">
                                <div class="row">
                                    <?= form_open('admin/equipments/update/' . $equipment['id'], array('class' => 'form-group')); ?>
                                    <div class="col-xs-12 col-sm-9">
                                        <div class="form-group">
                                            <label>Nome</label>
                                            <input type="text" class="form-control" name="name" value="<?= @$equipment['name'] ?>" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Nome completo</label>
                                            <input type="text" class="form-control" name="full_name" value="<?= @$equipment['full_name'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group is-required">
                                            <label>Marca</label>
                                            <input type="text" class="form-control" name="brand" value="<?= @$equipment['brand'] ?>" required>
                                        </div>
                                        <div class="form-group is-required">
                                            <label>Modelo</label>
                                            <input type="text" class="form-control" name="model" value="<?= @$equipment['model'] ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-9">
                                        <div class="form-group">
                                            <label>Descrição</label>
                                            <textarea class="form-control editor" name="description">
                                                <?= @$equipment['description'] ?>
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Requisitos funcionais</label>
                                            <textarea class="form-control editor" name="requirements">
                                                <?= @$equipment['requirements'] ?>
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Caraterísticas técnicas</label>
                                            <textarea class="form-control editor" name="datasheet">
                                                <?= @$equipment['datasheet'] ?>
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Manuais</label>
                                            <textarea class="form-control editor" name="manuals">
                                                <?= @$equipment['manuals'] ?>
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Outra informação</label>
                                            <textarea class="form-control editor" name="other_info">
                                                <?= @$equipment['other_info'] ?>
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group">
                                            <label>Acessórios</label>
                                            <?php if ($accessories->isEmpty()) { ?>
                                                <div class="text-muted text-center margin-top-15">
                                                    <i class="fa fa-info-circle"></i> <span class="bold text-uppercase">Aviso</span><br/>
                                                    Não existem accessórios para este equipamento.
                                                </div>
                                            <?php } else { ?>
                                                <div class="row">
                                                    <?php foreach ($accessories as $accessory) { ?>
                                                        <div class="col-xs-6">
                                                            <div class="checkbox icheck">
                                                                <?php if (isset($selectedAccessories)) {
                                                                    if ($selectedAccessories->contains($accessory->id)) { ?>
                                                                        <input type="checkbox" name="accessories[]" value="<?= $accessory->id ?>" checked> <?= $accessory->name ?>
                                                                    <?php } else { ?>
                                                                        <input type="checkbox" name="accessories[]" value="<?= $accessory->id ?>"> <?= $accessory->name ?>
                                                                    <?php }
                                                                } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <hr/>
                                        <div class="form-group">
                                            <div class="btn-group">
                                                <?= form_button(array('type' => 'submit', 'class' => 'btn btn-primary', 'content' => lang('word.save'))); ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <?= form_close(); ?>
                            </div>
                            <div class="tab-pane" id="tab-fotos">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <?= form_open_multipart('admin/equipmentsPhotos/store/' . $equipment->id, array('class' => 'dropzone dz-clickable', 'method' => 'POST', 'id' => 'image-dropzone')); ?>
                                        <div class="dz-default dz-message"><span>Arraste imagens ou clique aqui para fazer upload</span></div>
                                        <?= form_close(); ?>
                                    </div>
                                    <div class="spacer-20"></div>
                                    <h4 class="page-header"><i class="fa fa-photo margin-right-10"></i>Fotos Carregadas (<span id="foto-count"><?= @$photos->count() ?></span>)</h4>
                                    <?php include('partials/photos.php') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
