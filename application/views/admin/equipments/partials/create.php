<div id="create" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Criar Equipamento</h3>
            </div>
            <?= form_open('admin/equipments/store', array('class' => 'form-group')); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group is-required">
                            <label>Marca</label>
                            <input type="text" class="form-control" name="brand" value="<?= @$equipment['brand'] ?>" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group is-required">
                            <label>Modelo</label>
                            <input type="text" class="form-control" name="model" value="<?= @$equipment['model'] ?>" required>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group is-required">
                            <label>Categoria</label>
                            <select name="category_id" class="form-control" required>
                            <?php foreach ($categories as $category) { ?>
                                <option value="<?= $category->id ?>"><?= $category->name ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?= form_button(array('type' => 'submit', 'class' => 'btn btn-primary pull-left', 'content' => lang('word.save'))); ?>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
</div>