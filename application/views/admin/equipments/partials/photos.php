<?php
?>

<div class="row" id="photos">
    <?php if ($photos->count() > 0) { ?>
        <ul class="list-unstyled list-inline list-photos">
            <?php foreach ($photos as $photo) { ?>
                <li class="inline">
                    <a href="<?= base_url() . $photo->fileurl ?>" class="image-item" data-sub-html="<?= $photo->filename ?>">
                        <img src="<?= base_url() . $photo->fileurl ?>" alt="<?= $photo->filename ?>" width="200px" max-height="200px" class="padding-5px">
                    </a>
                    <div class="text-center">
                        <?= anchor($photo->fileurl, '<i class="fa fa-eye"></i>', array('class' => 'btn btn-default', 'target' => '_blank')); ?>
                        <?= anchor('admin/equipmentsPhotos/destroy/' . $photo->equipment_id . '/' . $photo->id, '<i class="fa fa-remove"></i>', array('class' => 'btn btn-danger margin-left-5')); ?>
                    </div>
                </li>
            <?php } ?>
        </ul>
    <?php } else { ?>
        <div class="text-muted text-center empty-photos" id="emptyPhotos">
            <i class="fa fa-info-circle"></i><h4>Ainda não existem fotos</h4>
        </div>
    <?php } ?>
</div>



