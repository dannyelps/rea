<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?= $pagetitle; ?>
        <?= $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create"><i class="fa fa-plus"></i> Novo Equipamento</button>
                        </h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped width-100" id="datatables">
                            <thead>
                            <tr>
                                <th style="width: 1%">#</th>
                                <th class="width-50px text-center"><i class="fa fa-camera"></i></th>
                                <th class="width-90px">Marca<br/><small>Modelo</small></th>
                                <th>Nome</th>
                                <th class="width-60px">Categoria</th>
                                <th class="width-70px">Estado</th>
                                <th class="width-70px"><?= lang('word.created_at'); ?></th>
                                <th class="width-90px"><?= lang('word.actions'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php include('partials/create.php'); ?>
    </section>
</div>


<script>
    $(document).ready(function () {

        var oTable = $('#datatables');

        oTable.DataTable({
            // "processing": true,
            // "serverSide": true,
            "ajax": {
                url: "<?= site_url() . 'admin/equipments/datatables' ?>",
                type: "POST",
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>'
                },
                dataSrc: function ( json ) {
                    if(json.csrf_token !== undefined) $('meta[name=_token]').attr("content", json.csrf_token);
                    return json.data;
                },
                error: function () {  // error handling
                    oTable.html("");
                    oTable.width('100%');
                    oTable.append('<tbody><tr><th><i class="fa fa-warning text-red" style="margin-right: 5px;"></i><?= lang('error_datatables_loading_data') ?></th></tr></tbody>');
                },
            },
            "columns": [
                {"data": "0"},
                {"data": "1", 'orderable': false},
                {"data": "2"},
                {"data": "3"},
                {"data": "4", class:'text-center'},
                {"data": "5", class:'text-center'},
                {"data": "6", class:'text-justify'},
                {"data": "7", 'orderable': false}
            ]
        });
    });
</script>
