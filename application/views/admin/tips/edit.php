<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<script src="https://cdn.ckeditor.com/ckeditor5/10.0.1/classic/ckeditor.js"></script>
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/dist/css/select2.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/select2/dist/css/select2-bootstrap.min.css'); ?>">
<script src="<?php echo base_url($plugins_dir . '/select2/dist/js/select2.min.js'); ?>"></script>
<script>
    $(function () {
        $('.select2').select2({
            theme: "bootstrap",
            width: '100%',
            placeholder: "Sem Catálogo"
        });
    });
</script>

<div class="content-wrapper">
    <section class="content-header">
        <?= $pagetitle; ?>
        <?= $breadcrumb; ?>

    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box no-border">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-right">
                            <li role="navigation">
                                <a href="#tab-fotos" data-toggle="tab" aria-expanded="true">Fotografias</a>
                            </li>
                            <li role="navigation" class="active">
                                <a href="#tab-info" data-toggle="tab" aria-expanded="false">Detalhes</a>
                            </li>

                            <li class="pull-left header">
                                Editar Dica
                            </li>
                        </ul>
                    </div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-info">
                                <div class="row">
                                    <?= form_open('admin/tips/update/' . $tip['id'], array('class' => 'form-group')); ?>
                                    <div class="col-xs-12 col-md-9">
                                        <div class="form-group is-required">
                                            <label>Nome</label>
                                            <input type="text" class="form-control" name="name" value="<?= @$tip['name'] ?>" required>
                                        </div>
                                        <div class="form-group is-required">
                                            <label>Descrição</label>
                                            <textarea class="form-control editor" style="height: 100px" rows="3" name="description">
                                        <?= @$tip['description'] ?>
                                    </textarea>
                                        </div>
                                        <div class="form-group is-required">
                                            <label>Conteúdo</label>
                                            <textarea class="form-control editor" style="height: 100px" name="text">
                                        <?= @$tip['text'] ?>
                            </textarea>
                                        </div>
                                        <div class="btn-group">
                                            <?= form_button(array('type' => 'submit', 'class' => 'btn btn-primary', 'content' => lang('word.save'))); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-3">
                                        <div class="form-group">
                                            <label>Catálogo</label>
                                            <select class="select2" name="catalog_id">
                                                <option value="0"><span class="text-muted">(Sem Catálogo)</span></option>
                                                <?php foreach ($catalogs as $catalog) {
                                                    if ($catalog->id == $tip->catalog_id) {?>
                                                        <option value="<?= $catalog->id ?>" selected><?= $catalog->name ?></option>
                                                        <?php } else { ?>
                                                        <option value="<?= $catalog->id ?>"><?= $catalog->name ?></option>
                                                <?php } }?>
                                            </select>
                                        </div>
                                    </div>
                                    <?= form_close(); ?>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-fotos">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <?= form_open_multipart('admin/tipsPhotos/store/' . $tip->id, array('class' => 'dropzone dz-clickable', 'method' => 'POST', 'id' => 'image-dropzone')); ?>
                                        <div class="dz-default dz-message"><span>Arraste imagens ou clique aqui para fazer upload</span></div>
                                        <?= form_close(); ?>
                                    </div>
                                    <div class="text-center">
                                        <!--                                        --><?php //redirect('admin/tips/edit/' . $tip->id . '#tab-fotos', 'refresh'); ?>
                                    </div>
                                    <div class="spacer-20"></div>
                                    <h4 class="page-header"><i class="fa fa-photo margin-right-10"></i>Fotos Carregadas (<span id="foto-count"><?= $photos->count() ?></span>)</h4>
                                    <?php include('partials/photos.php') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
