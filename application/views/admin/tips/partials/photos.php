<?php
?>

<div class="row" id="photos">
    <ul class="list-unstyled list-inline">
        <?php foreach ($photos as $photo) { ?>
        <li class="inline">
            <a href="<?= base_url() . $photo->fileurl ?>" class="image-item" data-sub-html="<?= $photo->filename ?>">
                <img src="<?= base_url() . $photo->fileurl ?>" alt="<?= $photo->filename ?>" width="200px" max-height="200px">
            </a>
            <div class="text-center">
                <?= anchor($photo->fileurl, '<i class="fa fa-eye"></i>', array('class' => 'btn btn-default', 'target' => '_blank')); ?>
                <?= anchor('admin/TipsPhotos/destroy/' . $photo->tips_id . '/' . $photo->id, '<i class="fa fa-remove"></i>', array('class' => 'btn btn-danger')); ?>
            </div>
            <?php } ?>
    </ul>
</div>


