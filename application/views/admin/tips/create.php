<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?= $pagetitle; ?>
        <?= $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nova Dica</h3>
                    </div>
                    <?php @$data = @$this->session->flashdata('data') ?>
                    <?= form_open('admin/tips/store', array('class' => 'form-group')); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="name" value="<?= @$data['name'] ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Descrição</label>
                            <textarea class="form-control" rows="3" name="description">
                                        <?= @$data['description'] ?>
                                    </textarea>
                        </div>
                        <div class="form-group">
                            <label>Conteúdo</label>
                            <textarea class="form-control" style="height: 100px" id="editor" name="text">
                                        <?= @$data['text'] ?>
                            </textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="btn-group">
                            <?= form_button(array('type' => 'submit', 'class' => 'btn btn-primary', 'content' => lang('word.save'))); ?>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
