<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!-- Plugin need -->
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/icheck/css/blue.css'); ?>">
<script src="<?php echo base_url($plugins_dir . '/icheck/js/icheck.min.js'); ?>"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
    });
</script>


<div class="content-wrapper">
    <section class="content-header">
        <?= $pagetitle; ?>
        <?= $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Editar Utilizador</h3>
                    </div>
                    <?= form_open('admin/users/update/' . $user['id'], array('class' => 'form-group')); ?>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label>Nome</label>
                                    <input type="text" class="form-control" name="name" value="<?= @$user['name'] ?>"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" value="<?= @$user['email'] ?>"
                                           disabled>
                                </div>
                                <?php if (($this->ion_auth->row()->user_id == $user->id) || $this->ion_auth->is_dev()) { ?>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password">
                                    </div>
                                <?php } ?>
                            </div>
                            <?php if (isset($permissions)) { ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Permissões</label>
                                        <?php if ($permissions->isEmpty()) { ?>
                                            <div class="text-muted text-center margin-top-15">
                                                <i class="fa fa-info-circle"></i> <span class="bold text-uppercase">Aviso</span><br/>
                                                Sem Permissões
                                            </div>
                                        <?php } else { ?>
                                            <div class="row">
                                                <?php foreach ($permissions as $permission) { ?>
                                                    <div class="col-xs-12">
                                                        <div class="checkbox icheck">
                                                            <?php if (isset($permissions_user)) {
                                                                if ($permissions_user->contains($permission->id)) { ?>
                                                                    <input type="checkbox" name="permissions[]"
                                                                           value="<?= $permission->id ?>"
                                                                           checked> <?= $permission->name ?>
                                                                <?php } else { ?>
                                                                    <input type="checkbox" name="permissions[]"
                                                                           value="<?= $permission->id ?>"> <?= $permission->name ?>
                                                                <?php }
                                                            } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="box-footer">
                        <?= form_hidden($_token); ?>
                        <?= form_button(array('type' => 'submit', 'class' => 'btn btn-primary', 'content' => lang('word.save'))); ?>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
