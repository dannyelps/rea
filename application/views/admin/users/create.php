<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?= $pagetitle; ?>
        <?= $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= lang('user_create'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?php @$user = @$this->session->flashdata('user') ?>
                        <?= form_open('admin/users/store', array('class' => 'form-group')); ?>
                        <div class="form-group">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="name" value="<?= $user['name'] ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" value="<?= $user['email'] ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" required>
                        </div>
                        <div class="form-group">
                            <label>Confirmar Password</label>
                            <input type="password" class="form-control" name="check_password" required>
                        </div>
                        <div class="form-group">
                            <div class="btn-group">
                                <?= form_button(array('type' => 'submit', 'class' => 'btn btn-primary', 'content' => lang('word.save'))); ?>
                            </div>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
