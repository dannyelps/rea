<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?= $pagetitle; ?>
        <?= $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <?= anchor('admin/accessories/create', '<i class="fa fa-plus"></i> Novo Acessório', array('class' => 'btn btn-block btn-primary btn-flat')); ?>
                        </h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped width-100" id="datatables">
                            <thead>
                            <tr>
                                <th style="width: 1%">#</th>
                                <th>Nome</th>
                                <th class="width-70px">Categoria</th>
                                <th class="width-70px"><?= lang('word.created_at'); ?></th>
                                <th class="width-70px"><?= lang('word.actions'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script>
    $(document).ready(function () {

        $('#datatables').DataTable({
            // "processing": true,
            // "serverSide": true,
            "ajax": {
                url: "<?= site_url() . 'admin/accessories/datatables' ?>",
                type: "POST",
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>' : '<?= $this->security->get_csrf_hash(); ?>'
                },
                dataSrc: function ( json ) {
                    if(json.csrf_token !== undefined) $('meta[name=_token]').attr("content", json.csrf_token);
                    return json.data;
                },
                error: function () {  // error handling
                    oTable.html("");
                    oTable.width('100%');
                    oTable.append('<tbody><tr><th><i class="fa fa-warning text-red" style="margin-right: 5px;"></i><?= lang('error_datatables_loading_data') ?></th></tr></tbody>');
                },
            },
            "order": [[ 1, "asc" ]],
            "columns": [
                {"data": "0"},
                {"data": "1"},
                {"data": "2", class:'text-justify'},
                {"data": "3", class:'text-justify'},
                {"data": "4"}
            ]
        });
    });
</script>
