<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!-- Plugin need -->
<link rel="stylesheet" href="<?php echo base_url($plugins_dir . '/icheck/css/blue.css'); ?>">
<script src="<?php echo base_url($plugins_dir . '/icheck/js/icheck.min.js'); ?>"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
    });
</script>


<div class="content-wrapper">
    <section class="content-header">
        <?= $pagetitle; ?>
        <?= $breadcrumb; ?>

    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box no-border">
                    <div class="box-header">
                        <h3 class="box-title"> <?= $action ?></h3>
                    </div>
                    <div class="box-body">
                        <?= form_open($formRoute, array('class' => 'form-group')); ?>
                        <div class="form-group is-required">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="name" value="<?= @$accessory['name'] ?>" required>
                        </div>
                        <?php if(isset($categories)) { ?>
                        <div class="form-group">
                            <label>Categorias</label>
                            <?php foreach (@$categories as $category) { ?>
                                <div class="checkbox icheck">
                                    <?php if (isset($selectedCategories)) {
                                        if ($selectedCategories->contains($category->id)) { ?>
                                            <input type="checkbox" name="categories[]" value="<?= $category->id ?>" checked> <?= $category->name ?>
                                        <?php } else { ?>
                                            <input type="checkbox" name="categories[]" value="<?= $category->id ?>"> <?= $category->name ?>
                                        <?php }
                                    } else { ?>
                                        <input type="checkbox" name="categories[]" value="<?= $category->id ?>"> <?= $category->name ?>

                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="box-footer">
                        <?= form_button(array('type' => 'submit', 'class' => 'btn btn-primary', 'content' => lang('word.save'))); ?>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
