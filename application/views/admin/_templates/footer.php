<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$result = $this->session->flashdata('result');
if ($result['result'] == true) {
    if ($result['type'] == 'success') {
        $result['icon'] = "<i class='fa fa-check'></i>";
    } else if ($result['type'] == 'error') {
        $result['icon'] = "<i class='fa fa-exclamation-circle'></i>";
    } else if ($result['type'] == 'warning') {
        $result['icon'] = "<i class='fa fa-warning'></i>";
    } else if ($result['type'] == 'info') {
        $result['icon'] = "<i class='fa fa-info-circle'></i>";
    }
}
?>

<footer class="main-footer">
    <strong> &copy; <?= date('Y'); ?> Dev. </strong>
        <div class="pull-right">
            <b><?= lang('footer_version'); ?></b> <?= $version ?>
        </div>
</footer>
</div>
<script>
    var x = <?= isset($result['result']) ? 'true' : 'false' ?>;
    $(document).ready(function () {
        if (x) {
            $.bootstrapGrowl("<?= $result['icon'] . " " . trim(preg_replace('/\s+/', ' ', $result['feedback'])) ?>", {type: '<?= $result['type'] ?>', align: 'center', width: 'auto', delay: 8000});
        }
    });
</script>
</body>
</html>