<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<header class="main-header">
    <a href="<?php echo site_url('admin/dashboard'); ?>" class="logo">
        <span class="logo-mini"><?= $title_mini; ?></span>
        <span class="logo-lg"><?= $title_lg; ?></span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account -->
                <li>
                    <a href="<?= site_url('/'); ?>" data-toggle="tooltip" title="Home" data-placement="bottom">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('/admin/users/profile/' . $user_login->id); ?>" data-toggle="tooltip" title="Ver Perfil" data-placement="bottom">
                        <img src="<?= base_url($avatar_dir . '/m_001.png'); ?>" class="user-image" alt="User Image" width="20px">
                        <span class="hidden-xs"><?= $user_login->name; ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('auth/logout/admin'); ?>" data-toggle="tooltip" title="Sair" data-placement="bottom"><i class="fa fa-power-off"></i></a>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li><a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a></li>
            </ul>
        </div>
    </nav>
</header>
