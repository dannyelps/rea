<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar menu -->
        <ul class="sidebar-menu">
            <li class="<?= active_link_controller('dashboard') ?>">
                <a href="<?= site_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i> <span><?= lang('menu_dashboard'); ?></span>
                </a>
            </li>
            <?php if(Category::get()->count() > 0) { ?>
            <li class="<?= active_link_controller('accessories') ?>">
                <a href="<?= site_url('admin/accessories'); ?>">
                    <i class="fa fa-wrench"></i> <span><?= lang('word.accessories'); ?></span>
                </a>
            </li>
            <?php } ?>
            <li class="<?= active_link_controller('categories') ?>">
                <a href="<?= site_url('admin/categories'); ?>">
                    <i class="fa fa-book"></i> <span><?= lang('word.categories'); ?></span>
                </a>
            </li>
            <li class="<?= active_link_controller('tips') ?>">
                <a href="<?= site_url('admin/tips'); ?>">
                    <i class="fa fa-lightbulb-o"></i> <span><?= lang('word.tips'); ?></span>
                </a>
            </li>
            <?php if(Category::get()->count() > 0) { ?>
            <li class="<?= active_link_controller('equipments') ?>">
                <a href="<?= site_url('admin/equipments'); ?>">
                    <i class="fa fa-camera"></i> <span>Equipamentos</span>
                </a>
            </li>
            <?php } ?>
            <?php if(Equipment::get()->count() > 0) { ?>
                <li class="<?= active_link_controller('catalogs') ?>">
                    <a href="<?= site_url('admin/catalogs'); ?>">
                        <i class="fa fa-list"></i> <span>Catálogos</span>
                    </a>
                </li>
            <?php } ?>

            <?php if($this->ion_auth->is_admin()) { ?>
                <li class="header text-uppercase"><?= lang('menu_administration'); ?></li>

            <li class="<?= active_link_controller('users') ?>">
                <a href="<?= site_url('admin/users'); ?>">
                    <i class="fa fa-user"></i> <span><?= lang('menu_users'); ?></span>
                </a>
            </li>
            <li class="<?= active_link_controller('groups') ?>">
                <a href="<?= site_url('admin/permissions'); ?>">
                    <i class="fa fa-shield"></i> <span><?= lang('menu_groups'); ?></span>
                </a>
            </li>
            <?php } ?>

            <li class="header text-uppercase"><?= lang('development'); ?></li>
            <li class="<?= active_link_controller('database') ?>">
                <a href="<?= site_url('admin/database'); ?>">
                    <i class="fa fa-database"></i> <span><?= lang('menu_database_utility'); ?></span>
                </a>
            </li>
            <li class="<?= active_link_controller('logs') ?>">
                <a href="<?= site_url('admin/logs'); ?>">
                    <i class="fa fa-cogs"></i> <span><?= lang('logs'); ?></span>
                </a>
            </li>
            <li class="<?= active_link_controller('reports') ?>">
                <a href="<?= site_url('admin/reports'); ?>">
                    <i class="fa fa-flag"></i> <span>Problemas</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
