<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->

    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane active" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Informações</h3>

            <ul class="control-sidebar-menu">
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-info bg-orange"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">2018 - 07 - 07</h4>
                            <p>Dicas lançado em beta</p>
                        </div>
                    </a>
                </li>
            </ul>

            <ul class="control-sidebar-menu">
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-calendar bg-aqua"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">2018 - 07 - 06</h4>
                            <p>Problema com o upload de imagens resolvido</p>
                        </div>
                    </a>
                </li>
            </ul>

            <ul class="control-sidebar-menu">
                <li>
                    <a href="#">
                        <i class="menu-icon fa fa-calendar bg-aqua"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">2018 - 07 - 05</h4>
                            <p>Tokens reativos<br/>
                            Sessões de 1 Hora <br/>
                            Corrigido erro ao editar equipamento</p>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</aside>

<div class="control-sidebar-bg"></div>
