<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!doctype html>
<html lang="<?=  $lang; ?>">
<head>
    <meta charset="<?=  $charset; ?>">
    <title><?=  $title; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="google" content="notranslate">
    <meta name="robots" content="noindex, nofollow">

    <link rel="icon"
          href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAqElEQVRYR+2WYQ6AIAiF8W7cq7oXd6v5I2eYAw2nbfivYq+vtwcUgB1EPPNbRBR4Tby2qivErYRvaEnPAdyB5AAi7gCwvSUeAA4iis/TkcKl1csBHu3HQXg7KgBUegVA7UW9AJKeA6znQKULoDcDkt46bahdHtZ1Por/54B2xmuz0uwA3wFfd0Y3gDTjhzvgANMdkGb8yAyY/ro1d4H2y7R1DuAOTHfgAn2CtjCe07uwAAAAAElFTkSuQmCC">
<!--    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,700italic">-->
    <link rel="stylesheet" href="<?=  base_url($frameworks_dir . '/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?=  base_url($frameworks_dir . '/font-awesome/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?=  base_url($frameworks_dir . '/ionicons/css/ionicons.min.css'); ?>">
    <link rel="stylesheet" href="<?=  base_url($frameworks_dir . '/adminlte/css/adminlte.min.css'); ?>">
    <link rel="stylesheet" href="<?=  base_url($frameworks_dir . '/adminlte/css/skins/skin-blue.min.css'); ?>">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?=  base_url($frameworks_dir . '/domprojects/css/dp.min.css'); ?>">
    <link rel="stylesheet" href="<?=  base_url($plugins_dir . '/dropzone/dropzone.css'); ?>">
    <link rel="stylesheet" href="<?=  base_url( $assets_dir . '/css/helper.css'); ?>">
    <link rel="stylesheet" href="<?=  base_url( $assets_dir . '/admin/css/app.css'); ?>">


    <script src="<?=  base_url($frameworks_dir . '/jquery/jquery.min.js'); ?>"></script>
    <script src="<?=  base_url($frameworks_dir . '/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?=  base_url($frameworks_dir . '/adminlte/js/adminlte.min.js'); ?>"></script>
    <script src="<?=  base_url($frameworks_dir . '/domprojects/js/dp.min.js'); ?>"></script>
    <script src="<?=  base_url($plugins_dir . '/slimscroll/slimscroll.min.js'); ?>"></script>
    <script src="<?=  base_url( $plugins_dir . '/bootstrap-growl/jquery.bootstrap-growl.min.js'); ?>"></script>
    <script src="<?=  base_url( $plugins_dir . '/ckeditor5/ckeditor.js'); ?>"></script>
    <script src="<?=  base_url( $plugins_dir . '/dropzone/dropzone.js'); ?>"></script>
    <script src="<?=  base_url($plugins_dir . '/animsition/animsition.min.js'); ?>"></script>
    <script src="<?=  base_url($assets_dir . '/admin/js/app.js'); ?>"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>

    <script>
        $.extend(true, $.fn.dataTable.defaults,  {
            "oLanguage": {
                "sProcessing": "A processar...",
                "sLengthMenu": "Mostrar _MENU_ registos",
                "sZeroRecords": "<span class='text-muted'><i class='fa fa-info-circle'> Não foram encontrados resultados.</i></span>",
                "sInfo": "A mostrar de _START_ até _END_ de _TOTAL_ registos",
                "sInfoEmpty": "A mostrar de 0 até 0 de 0 registos",
                "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                "sInfoPostFix": "",
                "sEmptyTable": "<span class='text-muted'><i class='fa fa-info-circle'> Não existem dados a apresentar.</i></span>",
                "sLoadingRecords": "<span class='fa fa-circle-o-notch fa-spin'></span> A carregar dados...",
                "sSearch": "<span class='fa fa-search'></span>",
                "sSearchPlaceholder": "Procurar ..",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Primeira",
                    "sPrevious": "Anterior",
                    "sNext": "Seguinte",
                    "sLast": "Última",
                }
            }
        });
    </script>
</head>
<body class="hold-transition skin-blue fixed sidebar-mini sidebar-collapse">
<div class="wrapper">