<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
    <section class="content-header">
        <?= $pagetitle; ?>
        <?= $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-9">
                <div class="box no-border">
                    <div class="box-header">
                        <h3 class="box-title"> Editar Catálogo</h3>
                    </div>
                    <?= form_open('admin/reports/update/'. $report['id'], array('class' => 'form-group')); ?>
                    <div class="box-body">
                        <div class="form-group is-required">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="name" value="<?= @$report['name'] ?>" required>
                        </div>
                        <div class="form-group is-required">
                            <label>Descrição</label>
                            <textarea class="form-control editor" name="description" required> <?= @$report['description'] ?></textarea>
                        </div>

                        <div class="form-group is-required">
                            <label>Estado</label>
                            <?php if($this->ion_auth->in_group('dev')) { ?>
                                <input type="text" class="form-control" name="status" value="<?= @$report['status'] ?>" required>
                            <?php } else { ?>
                                <input type="text" class="form-control" name="status" value="<?= @$report['status'] ?>" disabled>
                            <?php } ?>
                        </div>
                        <?= form_hidden('is_read', $report['is_read']); ?>
                    </div>
                    <div class="box-footer">
                        <?= form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('word.save'))); ?>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Print</label>
                    <?php if (!$report->fileurl) { ?>
                        <div class="text-muted text-center">
                            <i class="fa fa-info-circle"></i><h4>Não foi adicionado</h4>
                        </div>
                    <?php } else { ?>
                        <div class="image thumbnail">
                            <a href="<?= base_url() . $report->fileurl ?>" class="image-item" data-sub-html="<?= $report->filename ?>">
                                <img src="<?= base_url() . $report->fileurl ?>" alt="<?= $report->filename ?>" width="200px" height="200px">
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</div>

