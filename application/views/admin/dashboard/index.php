<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">

    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <?php $this->load->view('admin/partials/error.php'); ?>
    <section class="content">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-flag"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Problemas Encontrados</span>
                        <span class="info-box-number"><?= $count_reports ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-orange"><i class="fa fa-check"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Equipamentos</span>
                        <span class="info-box-number"><?= $count_equipment ?></span>
                    </div>
                </div>
            </div>

            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Users</span>
                        <span class="info-box-number"><?= $count_users; ?></span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="callout callout-warning">
                    <h4>Beta</h4>
                    <ul>
                        <li>Dicas em teste</li>
                    </ul>
                </div>
                <div class="callout callout-info">
                    <h4>Alterações!</h4>
                    <ul>
                        <li>As alterações passam a estar disponíveis no menu lateral do lado direito<br/>
                            <small>( Para aceder basta carregar no icon no canto superior direito )</small>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Reportar</h3>
                    </div>
                    <?= form_open('admin/reports/store', array('class' => 'form-group')); ?>
                    <div class="box-body">
                        <div class="form-group is-required">
                            <label>Erro/Bug</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                        <div class="form-group is-required">
                            <label>Descrição</label>
                            <textarea name="description" class="form-control editor"></textarea>
                        </div>
                        <input type="file" name="file" size="20" class="btn btn-default width-100"/>
                        <?= form_hidden('user_id', $user_login->id); ?>
                    </div>
                    <div class="box-footer clearfix">
                        <?= form_button(array('type' => 'submit', 'class' => 'btn btn-primary pull-right', 'content' => '<i class="fa fa-fw fa-send"></i> Enviar')); ?>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Problemas</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php if ($reports->isEmpty()) { ?>
                            <span class="text-muted">Sem problemas novos</span>
                        <?php } else { ?>
                            <ul class="products-list product-list-in-box">
                                <?php foreach ($reports as $report) { ?>
                                    <li class="item">
                                        <div class="product-img">
                                            <?php if ($report->fileurl) { ?>
                                                <img src="<?= $report->fileurl ?>" alt="<?= $report->name ?>">
                                            <?php } else { ?>
                                                <img src="<?= base_url() . 'assets/img/default.svg' ?>"
                                                     alt="<?= $report->name ?>">
                                            <?php } ?>
                                        </div>
                                        <div class="product-info">
                                            <?php if ($report->isread = 0) { ?>
                                                <?= anchor('admin/reports/edit/' . $report->id, $report->name, array('class' => '')) ?>
                                            <?php } else { ?>
                                                <?= anchor('admin/reports/edit/' . $report->id, $report->name, array('class' => 'text-bold')) ?>
                                            <?php } ?>
                                            <span class="label label-default pull-right"><?= $report->status ?></span>
                                            <span class="product-description">
                                        <?= str_limit($report->description, 150) ?>
                                    </span>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="<?= site_url('admin/reports'); ?>"> Ver lista de Problemas</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
            <?php if ($this->ion_auth->is_dev()) { ?>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">
                                <i class="fa fa-clock-o"></i> Últimos Logins
                            </h3>
                            <span class="pull-right">
                                <small>
                                        Actualizado às <?= \Carbon\Carbon::now()->toTimeString(); ?>
                                </small>
                            </span>
                        </div>
                        <div class="box-body">
                            <table class="table table-schedule">
                                <tbody>
                                <?php foreach ($logins as $login) { ?>
                                    <tr>
                                        <td class="width-40"><b><?= $login->name ?></b></td>
                                        <td class="width-30"><?= Carbon\Carbon::createFromTimestamp($login->last_login)->diffForHumans(); ?></td>
                                        <td class="width-30 text-right"><?= \Carbon\Carbon::createFromTimestamp($login->last_login)->toDateTimeString(); ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>
</div>
