<div class="row">
    <div class="col-xs-12 col-md-3 text-center-xs text-center-sm    ">
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>"><i class="fa fa-home"></i></a></li>
            <li><a href="<?= site_url('equipamentos') ?>">Equipamentos</a></li>
        </ol>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="input-group-lg">
            <div class="inner-addon right-addon">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-info fa-stack-1x fa-inverse"></i>
                </span>
                <input type="text" class="form-control form-group-lg" placeholder="Tipo de Trabalho / Condições, .."
                       id="txt_name" value="<?= @$search ?>"/>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3 text-right text-center-xs text-center-sm">
        <div class="spacer-10 visible-xs visible-sm"></div>
        <a id="search" class="btn btn-gold btn-lg">Procurar
        </a>
        <a href="<?= site_url('equipamentos') ?>" class="btn btn-gray btn-lg margin-left-5">
            Ver tudo
        </a>
    </div>
</div>
<div class="row row-10">
    <div class="spacer-30"></div>
    <?php if ($equipments->isEmpty()) { ?>
        <div class="col-xs-12">
            <div class="text-muted text-center">
                <i class="fa fa-info-circle"></i><br/>
                 Ainda não foram adicionadas dicas
            </div>
        </div>
    <?php } else { ?>
        <?php foreach ($equipments as $equipment) { ?>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <a href="<?= site_url('equipamentos/show/' . $equipment->slug) ?>">
                    <div class="tip">
                        <div id="carousel-<?= $equipment->id ?>" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <?php $x = 0; ?>
                                <?php foreach ($equipmentsImgs as $img) { ?>
                                    <?php if ($x == 0) { ?>
                                        <li data-target="#carousel-<?= $equipment->id ?>" data-slide-to="<?= $x ?>"
                                            class="active"></li>
                                        <?php $x++;
                                    } else { ?>
                                        <li data-target="#carousel-<?= $equipment->id ?>" data-slide-to="<?= $x ?>"></li>
                                    <?php } ?>
                                <?php } ?>
                            </ol>
                            <div class="carousel-inner">
                                <?php if ($equipmentsImgs->contains($equipment->id)) { ?>
                                <?php $i = 0; ?>
                                <?php foreach ($equipmentsImgs as $img) { ?>
                                <?php if ($i == 0) {
                                $i++; ?>
                                <div class="item active">
                                    <?php } else { ?>
                                    <div class="item">
                                        <?php } ?>
                                        <img src="<?= base_url() . $img->fileurl ?>"
                                             alt="<?= $img->filename ?>">
                                    </div>
                                    <?php } ?>
                                    <?php } else { ?>
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0"
                                                class="active"></li>
                                        </ol>
                                        <div class="item active">
                                            <img src="<?= base_url() . 'assets/img/default.svg' ?>"
                                                 alt="error">
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="info-tip">
                                <h4><?= $equipment->name ?></h4>
                                <p><?= str_limit($equipment->description, 150, '..') ?> </p>
                            </div>
                        </div>
                </a>
            </div>
        <?php } ?>
    <?php } ?>
    <!--    endforeach-->
</div>

<script type="text/javascript">
    $('#search').on('click', function () {
        window.location.href = "<?= base_url('equipamentos/search/'); ?>" + $('#txt_name').val();
    });
</script>
