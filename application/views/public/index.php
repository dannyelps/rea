<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!doctype html>
<html lang="<?= $lang; ?>">
<head>
    <meta charset="<?= @$charset; ?>">
    <title><?= @$title; ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="icon" href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAqElEQVRYR+2WYQ6AIAiF8W7cq7oXd6v5I2eYAw2nbfivYq+vtwcUgB1EPPNbRBR4Tby2qivErYRvaEnPAdyB5AAi7gCwvSUeAA4iis/TkcKl1csBHu3HQXg7KgBUegVA7UW9AJKeA6znQKULoDcDkt46bahdHtZ1Por/54B2xmuz0uwA3wFfd0Y3gDTjhzvgANMdkGb8yAyY/ro1d4H2y7R1DuAOTHfgAn2CtjCe07uwAAAAAElFTkSuQmCC">
    <link rel="stylesheet" href="<?= base_url($frameworks_dir . '/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url($frameworks_dir . '/font-awesome/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url($assets_dir . '/css/helper.css'); ?>">
    <link rel="stylesheet" href="<?= base_url($assets_dir . '/css/home.css'); ?>">

    <script src="<?= base_url($frameworks_dir . '/jquery/jquery.min.js'); ?>"></script>
    <script src="<?= base_url($frameworks_dir . '/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url($plugins_dir . '/bootstrap-growl/jquery.bootstrap-growl.min.js'); ?>"></script>
</head>
<body>
<header>
    <nav class="navbar navbar-default navbar-fixed-top blurry">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <li class="navbar-brand visible-xs"><?= $titleMini ?></li>
            </div>

            <!-- Collect the nav links, forms, and otoer content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav hidden-xs hidden-sm">
                    <li>
                        <a href="<?= base_url(); ?>"><h2><?= $title ?></h2></a>
                    </li>
                </ul>
                <ul class="nav navbar-nav visible-sm">
                    <li>
                        <a href="<?= base_url(); ?>"><h2><?= $titleMini ?></h2></a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="visible-xs">
                        <a href="#">Equipamentos</a>
                    </li>
                    <li class="visible-xs">
                        <a href="#">Dicas</a>
                    </li>

                    <li class="dropdown visible-xs">
                        <?php if (@$user_login) { ?>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i
                                        class="hidden-xs fa fa-fw fa-user"></i> <?= $user_login->name ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Histórico de Requisições</a></li>
                                <li><a href="<?= site_url('auth/logout'); ?>">Logout</a></li>
                            </ul>

                        <?php } else { ?>
                            <a href="#" data-toggle="modal" data-target="#login"> <i class="fa fa-user"></i> Login</a>
                        <?php } ?>
                    </li>
                    <li class="dropdown hidden-xs">
                        <?php if (@$user_login) { ?>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <h2><i class="fa fa-fw fa-user"></i> <?= $user_login->name ?> <span class="caret"></span></h2></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Histórico de Requisições</a></li>
                                <li><a href="<?= site_url('auth/logout'); ?>">Logout</a></li>
                            </ul>
                        <?php } else { ?>
                            <a href="#" data-toggle="modal" data-target="#login"><h2><i class="fa fa-user"></i> Login</h2></a>
                        <?php } ?>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>
<div class="div-bottom-right">
    <a href="#" class="btn btn-gold">Equipamentos</a>
    <br/>
    <a href="<?= site_url('dicas');?>" class="btn btn-gold">Dicas</a>
</div>

<!-- Modal -->
<div id="login" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
               <div class="row">
                   <div class="col-xs-12">
                       <h2 class="bold margin-bottom-15">Login</h2>
                       <h4 class="margin-bottom-25">Use a sua conta da ESTGV.</h4>
                       <?= form_open('auth/login/home');?>
                       <div class="form-group input-group-lg">
                           <input name="identity" type="email" placeholder="estgvXXXXX@praxis.pt" class="form-control">
                       </div>
                       <div class="form-group input-group-lg">
                           <input name="password" type="password" placeholder="password" class="form-control">
                       </div>
                       <?= form_submit('submit','Entrar', array('class' => 'btn btn-login btn-block bold margin-top-30'));?>
                       <?= form_close();?>
                   </div>
               </div>
            </div>
        </div>

    </div>
</div>
<footer>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p>
                        © Céline Direito - Daniel Seabra - João Sá - José Veiga
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php
$result = $this->session->flashdata('result');
if ($result['result'] == true) {
    if ($result['type'] == 'success') {
        $result['icon'] = "<i class='fa fa-check'></i>";
    }
    else if ($result['type'] == 'error') {
        $result['icon'] = "<i class='fa fa-exclamation-circle'></i>";
    }
    else if ($result['type'] == 'warning') {
        $result['icon'] = "<i class='fa fa-warning'></i>";
    }
    else if ($result['type'] == 'info') {
        $result['icon'] = "<i class='fa fa-info-circle'></i>";
    }
}
?>

<script>
    var x = <?= isset($result['result']) ? 'true' : 'false' ?>;
    $(document).ready(function () {
        if (x)
        {
            $.bootstrapGrowl("<?= $result['icon'] . " " . trim(preg_replace('/\s+/', ' ', $result['feedback'])) ?>", {type: '<?= $result['type'] ?>', align: 'center', width: 'auto', delay: 800000});
        }
    });
</script>
</body>
</html>
