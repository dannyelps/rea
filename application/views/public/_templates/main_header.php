<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<header>

    <div class="navbar navbar-default navbar-fixed-top custom-navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand text-uppercase hidden-xs hidden-sm" href="<?= base_url() ?>">
                    <?= $title ?>
                </a>
                <a class="navbar-brand text-uppercase visible-xs visible-sm" href="<?= base_url() ?>">
                    REA
                </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="pull-right visible-xs">
                    <a class="navbar-brand" href="#">
                        <i class="fa fa-user"></i>
                    </a>
                    <a class="navbar-brand margin-right-10" href="#">
                        <i class="fa fa-shopping-basket"></i>
                        <span class="shoping-cart btn btn-default btn-circle"><?= $this->cart->total_items() ?></span>
                    </a>
                </div>
            </div>

            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="visible-xs">
                        <a>Equipamentos</a>
                    </li>
                    <li class="visible-xs">
                        <a>Dicas</a>
                    </li>
                    <?php if ($this->ion_auth->logged_in()) { ?>
                        <li id="login">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-fw fa-user hidden-xs"></i><?= $user_login->name ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <?php if ($this->ion_auth->logged_in()) { ?>
                                    <li><a href="<?= site_url('admin'); ?>">Plataforma</a></li>
                                <?php } ?>
                                <li><a href="#">Histórico de Requisições</a></li>
                                <li><a href="<?= site_url('auth/logout'); ?>">Logout</a></li>
                            </ul>
                        </li>
                        <li id="calendar">
                            <a>
                            <i class="fa fa-calendar"></i>

                            </a>
                        </li>
                    <?php } else { ?>
                        <li id="login">
                            <a href="" title="Login"><i class="fa fa-fw fa-user"></i>Login</a>
                        </li>
                    <?php } ?>
                    <li class="hidden-xs" id="cart">
                        <a href="#"> <i class="fa fa-shopping-basket fa-fw"></i>
                            <span class="shoping-cart btn btn-default btn-circle"><?= $this->cart->total_items() ?></span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</header>
<div class="container">

