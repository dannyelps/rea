<div class="row">
    <div class="col-xs-12 text-center-xs">
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>"><i class="fa fa-home"></i></a></li>
            <li><a href="<?= site_url('dicas') ?>">Dicas</a></li>
            <li><a href="<?= site_url('dicas/show/' . $tip->slug) ?>"><?= $tip->name ?></a></li>
        </ol>
    </div>
</div>
<div class="spacer-20"></div>
<div class="row">
    <div class="col-xs-12 col-sm-8">
        <div class="card card-full" id="card-full">
            <?= $tip->text ?>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4" id="size">
    <div id="carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php $x = 0; ?>
            <?php foreach ($tipImgs as $img) { ?>
                <?php if ($x == 0) { ?>
                    <li data-target="#carousel" data-slide-to="<?= $x ?>" class="active"></li>
                    <?php $x++;
                } else { ?>
                    <li data-target="#carousel" data-slide-to="<?= $x ?>"></li>
                <?php } ?>
            <?php } ?>
        </ol>
        <div class="carousel-inner">
            <?php if ($tipImgs->contains($tip->id)) { ?>
            <?php $i = 0; ?>
            <?php foreach ($tipImgs
            as $img) { ?>
            <?php if ($i == 0) {
            $i++; ?>
            <div class="item active">
                <?php } else { ?>
                <div class="item">
                    <?php } ?>
                    <img src="<?= base_url() . $img->fileurl ?>" alt="<?= $img->filename ?>">
                </div>
                <?php } ?>
                <?php } else { ?>
                    <ol class="carousel-indicators">
                        <li data-target="#carousel" data-slide-to="0" class="active"></li>
                    </ol>
                    <div class="item active">
                        <img src="<?= base_url() . 'assets/img/default.svg' ?>"
                             alt="error">
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="spacer-20"></div>
        <div class="card">
            <?= $tip->description ?>
        </div>
        <div class="spacer-20"></div>
        <a onclick="brevemente()" class="btn btn-block btn-gold btn-lg">Equipamentos Sugeridos <i class="fa fa-info-circle"></i></a>
    </div>
</div>

<!--    set size of div -->
<script>
    $(document).ready(function () {
        var height = $("#size").height();
        document.getElementById("card-full").style.minHeight = height + "px";
    });
    function brevemente() {
        alert("Brevemente disponível!");
    }
</script>