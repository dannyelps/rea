<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Catalog extends Eloquent
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "catalogs"; // table name

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /*
     * RELATIONS
     */

    /**
     * Get the equipments for the catalogs.
     */
    public function equipments()
    {
        return $this->belongsToMany('Equipment', 'catalogs_equipments', 'catalog_id', 'equipment_id');
    }

}