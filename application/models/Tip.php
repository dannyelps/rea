<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Tip extends Eloquent
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "tips"; // table name


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'text', 'catalog_id'];


    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    | Define current model relationships
    */
    public function images()
    {
        return $this->hasMany('application\models\TipPhoto');
    }


    /*
    |--------------------------------------------------------------------------
    | Accessors & Mutators
    |--------------------------------------------------------------------------
    |
    | Eloquent provides a convenient way to transform your model attributes when
    | getting or setting them. Simply define a 'getFooAttribute' method on your model
    | to declare an accessor. Keep in mind that the methods should follow camel-casing,
    | even though your database columns are snake-case.
    |
    */

//    public function getSlug($name, $id) {
//        $name = strtolower($name);
//        $slug = str_slug($name, "-");
//        $params = array ();
//        $params['slug'] = $slug;
//        $i = 0;
//
//        while (Tip::where('slug', $slug)->where('id', '!=', $id)->first()) {
//            if (!preg_match('/-{1}[0-9]+$/', $slug)) {
//                $slug .= '-' . ++$i;
//            } else {
//                $slug = preg_replace('/[0-9]+$/', ++$i, $slug);
//            }
//            $params ['slug'] = $slug;
//        }
//
//        return $slug;
//    }
    /*
      |--------------------------------------------------------------------------
      | Scopes
      |--------------------------------------------------------------------------
      |
      | Scopes allow you to easily re-use query logic in your models.
      | To define a scope, simply prefix a model method with scope.
      |
     */

    /*
     * Show rows active
     */
    public function scopeIsActive($query)
    {
        return $query->where('is_active', 1);
    }
}