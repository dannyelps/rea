<?php

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
trait FileTrait
{

    //fileurl = 255;
    //filename = 100

    /**
     * Get temporary file path from object or $_FILES array
     *
     * @param object|array $file
     * @return string
     */
    protected static function getTempFilePath($file)
    {
        if (is_object($file)) {
            return $file->getRealPath();
        } else {
            return $file['tmp_name'];
        }
    }

    /**
     * Get news thumbnail
     * @return string
     */
    public function getThumb()
    {
        return $this->getCroppa(200, 180);
    }

    /**
     * Get cropped image
     * @param  integer $width
     * @param  integer $height [optional]
     * @param  array $options [optional]
     * @return string
     */
    public function getCroppa($width, $height = null, $options = null)
    {

        $config['image_library']  = 'gd2';
        $config['source_image']   = $this->fileurl;
        $config['maintain_ratio'] = TRUE;
        $config['width']          = $width;
        $config['height']         = $height;

        $this->load->library('image_lib', $config);

        return $this->image_lib->resize();
    }

    /**
     * Get large image
     * @return string
     */
    public function getLarge()
    {
        return $this->getCroppa(750);
    }

    /**
     *
     * @return string
     */
    public function getHumanFileSize($decimals = 2)
    {
        $size = array('1B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $factor = floor((strlen($this->getFileSize()) - 1) / 3);
        return sprintf("%.{$decimals}f", $this->getFileSize() / pow(1024, $factor)) . @$size[$factor];
    }

    /**
     *
     * @return int size in bytes
     */
    public function getFileSize()
    {
        return File::size($this->fileurl);
    }

    /**
     * Upload file to directory and store into database
     *
     * @param object $file
     * @return boolean
     */
    public function upload($file)
    {
        // Get absolute and relative path
        $path = $this->getFilePath($file);

        // Get absolute path information
        $absolutePathInfo = pathinfo($path['absolute']);

        // Store some fields related with uploaded file
        $this->filename = $file['name'];
        $this->fileurl = $path['relative'];

        $moved = false;

        try {
            // Check if exists directory, if not create it
            if (!is_dir($absolutePathInfo['dirname'])) {
                mkdir($absolutePathInfo['dirname'] . '/', 0755, true);
            }
            // Upload the file
            $moved = move_uploaded_file($file['tmp_name'], $absolutePathInfo['dirname'] . '/' . $absolutePathInfo['basename']);
        } catch (Exception $exception) {
            log_message('error', $exception);
        }

        // If file is realy uploaded, save this model
        if ($moved) {
            return $this->save();
        }

        return false;
    }

    /**
     * Get new path to upload document
     *
     * @param integer $id
     * @param array $file
     * @return string
     */
    protected static function getFilePath($file)
    {
        $fileName = self::getFileName($file);

        return array(
            'absolute' => self::getDirectoryBase() . '/' . $fileName,
            'relative' => self::DIRECTORY . '/' . $fileName,
        );
    }

    /**
     * Get random string to use as file name
     *
     * @param integer $id
     * @param array|object $file
     * @return string
     */
    protected static function getFileName($file)
    {
        $file['name'] = str_replace(' ', '_', $file['name']);
        return Str::lower(Str::random(8). '_' . $file['name']);
    }

    /**
     * Get path of documents directory
     *
     * @return string
     */
    protected static function getDirectoryBase()
    {
        //local
        return 'C:\wamp64\www\rea/' . self::DIRECTORY;
        //server
//        return self::DIRECTORY;
    }
}    