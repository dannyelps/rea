<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Accessory extends Eloquent
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "accessories"; // table name


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];


    /*
    |--------------------------------------------------------------------------
    | Accessors & Mutators
    |--------------------------------------------------------------------------
    |
    | Eloquent provides a convenient way to transform your model attributes when
    | getting or setting them. Simply define a 'getFooAttribute' method on your model
    | to declare an accessor. Keep in mind that the methods should follow camel-casing,
    | even though your database columns are snake-case.
    |
    */

    /**
     * Get the categories for the accessories.
     */
    public function categories()
    {
        return $this->belongsToMany('Category', 'categories_accessories', 'accessory_id', 'category_id');
    }

    /**
     * Get the categories for the accessories.
     */
    public function equipment()
    {
        return $this->belongsToMany('Equipment', 'equipments_accessories', 'accessory_id', 'equipment_id');
    }


    /*
      |--------------------------------------------------------------------------
      | Scopes
      |--------------------------------------------------------------------------
      |
      | Scopes allow you to easily re-use query logic in your models.
      | To define a scope, simply prefix a model method with scope.
      |
     */
    /*
    * Filter by category
    */

    public function scopeCategoryId($query, $id)
    {
        return $query->where('category_id', $id);
    }



}