<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class User extends Eloquent
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "users"; // table name

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'is_active', 'ip_address'];

    /**
     * The attributes don´t came in data
     *
     * @var array
     */
    protected $hidden = [
        'password', '_token',
    ];


    /*
     |--------------------------------------------------------------------------
     | Accessors & Mutators
     |--------------------------------------------------------------------------
     |
     | Eloquent provides a convenient way to transform your model attributes when
     | getting or setting them. Simply define a 'getFooAttribute' method on your model
     | to declare an accessor. Keep in mind that the methods should follow camel-casing,
     | even though your database columns are snake-case.
     |
     */
    public function setPasswordAttribute($pass){

        $this->attributes['password'] = password_hash($pass, PASSWORD_BCRYPT);

    }

    public function setIpAttribute($value)
    {
        $this->attributes['ip_address'] = ip2long($value);
    }

    /*
      |--------------------------------------------------------------------------
      | Scopes
      |--------------------------------------------------------------------------
      |
      | Scopes allow you to easily re-use query logic in your models.
      | To define a scope, simply prefix a model method with scope.
      |
     */

    /*
     * Show rows active
     */
    public function scopeIsActive($query)
    {
        return $query->where('is_active', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    | Define current model relationships
    */

    /* Get the categories from the accessories. */
    public function permission()
    {
        return $this->belongsToMany('Permission', 'users_permissions', 'user_id', 'permission_id');
    }
}