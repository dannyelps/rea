<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class TipPhoto extends Eloquent
{
    use SoftDeletes, FileTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "tips_img"; // table name


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tips_id', 'filename', 'fileurl', 'is_visible'];


    /**
     * Default directory
     *
     * @const string
     */
    const DIRECTORY = 'upload/tips';


    /*
 |--------------------------------------------------------------------------
 | Relationships
 |--------------------------------------------------------------------------
 |
 | Define current model relationships
 */

    public function tip()
    {
        return $this->belongsTo('application\models\admin\tip');
    }


    /*
    |--------------------------------------------------------------------------
    | Accessors & Mutators
    |--------------------------------------------------------------------------
    |
    | Eloquent provides a convenient way to transform your model attributes when
    | getting or setting them. Simply define a 'getFooAttribute' method on your model
    | to declare an accessor. Keep in mind that the methods should follow camel-casing,
    | even though your database columns are snake-case.
    |
    */


    /*
      |--------------------------------------------------------------------------
      | Scopes
      |--------------------------------------------------------------------------
      |
      | Scopes allow you to easily re-use query logic in your models.
      | To define a scope, simply prefix a model method with scope.
      |
     */

    /*
     * Show rows active
     */
    public function scopeIsVisible($query)
    {
        return $query->where('is_visible', 1);
    }
}