<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Equipment extends Eloquent
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "equipments"; // table name


    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /*
     * RELATIONS
     */

    /* Get the categories from the accessories. */
    public function accessories()
    {
        return $this->belongsToMany('Accessory', 'equipments_accessories', 'equipment_id', 'accessory_id');
    }

    /* Get the equipments from the catalogs. */
    public function catalogs()
    {
        return $this->belongsToMany('Catalog', 'catalogs_equipments', 'equipment_id', 'catalog_id');
    }

    /* Get the equipments from the catalogs. */
    public function categories()
    {
        return $this->belongsTo('Category', 'category_id');
    }

    /* Get images from equipment*/
    public function images()
    {
        return $this->hasMany('application\models\admin\EquipmentPhoto');
    }


    /*
      |--------------------------------------------------------------------------
      | Scopes
      |--------------------------------------------------------------------------
      |
      | Scopes allow you to easily re-use query logic in your models.
      | To define a scope, simply prefix a model method with scope.
      |
     */

    /*
     * Show rows active
     */

    public function scopeIsActive($query)
    {
        return $query->where('is_active', 1);
    }
}