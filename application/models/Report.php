<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Eloquent
{
    use SoftDeletes, FileTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "reports"; // table name


    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Default directory
     *
     * @const string
     */
    const DIRECTORY = 'upload/reports';

    /*
      |--------------------------------------------------------------------------
      | Scopes
      |--------------------------------------------------------------------------
      |
      | Scopes allow you to easily re-use query logic in your models.
      | To define a scope, simply prefix a model method with scope.
      |
     */

    /* Show rows only read */
    public function scopeIsRead($query)
    {
        return $query->where('is_read', 1);
    }

    /* Show rows not read */
    public function scopeIsNotRead($query)
    {
        return $query->where('is_read', 0);
    }
}