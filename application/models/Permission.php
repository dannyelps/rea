<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Permission extends Eloquent {

    protected $table = "permissions"; // table name


    public function user()
    {
        return $this->belongsToMany('User', 'users_permissions', 'permission_id', 'user_id');
    }
}