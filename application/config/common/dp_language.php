<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['language_abbr'] = array(
    'portuguese' => 'pt',
    'french'     => 'fr',
    'english'    => 'en'
);
